<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OTP extends Model
{
    use HasFactory;

    protected $table = 'otp';

    protected $fillable = [
        'user_id',
        'phone',
        'code',
        'expires_at',
        'verified_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
