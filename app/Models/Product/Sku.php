<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use database\factories\Product\SkusFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Order\Cart;
use App\Models\Promo\FlashsaleItems;

class Sku extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'product_id',
        'code',
        'image',
        'price',
        'properties',
        'stock',
        'weight_gram'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function getPropertiesAttribute($value)
    {
        return json_decode($value);
    }

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public function flashsaleItems()
    {
        return $this->hasMany(FlashsaleItems::class);
    }
}
