<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    use HasFactory;

    protected $table = "order_payment";

    protected $fillable = [
        'order_id',
        'total',
        'payment_method',
        'payment_status',
        'midtrans_snap_token'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
