<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Sku;
use App\Models\Product\Product;
use App\Models\User;

class Cart extends Model
{
    use HasFactory;

    protected $table = 'cart';

    protected $fillable = [
        'user_id',
        'sku_id',
        'quantity',
        'ip_address',
    ];

    public function sku()
    {
        return $this->belongsTo(Sku::class)->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
