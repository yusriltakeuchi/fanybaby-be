<?php

namespace App\Models\Order;

use App\Models\Promo\Voucher;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';

    protected $fillable = [
        'user_id',
        'voucher_id',
        'courier_id',
        'courier_services',
        'shipping_fee',
        'customer_id',
        'subtotal',
        'awb',
        'status',
        'total',
        'is_cod',
        'is_downloaded',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function voucher()
    {
        return $this->belongsTo(Voucher::class);
    }

    public function courier()
    {
        return $this->belongsTo(Courier::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItems::class);
    }

    public function orderPayment()
    {
        return $this->hasOne(OrderPayment::class);
    }

    public function orderHistory()
    {
        return $this->hasMany(OrderHistory::class)->orderByDesc('id');
    }
}
