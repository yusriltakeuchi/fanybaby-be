<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Location\Subdistrict;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'customer';

    protected $fillable = [
        'user_id',
        'title',
        'customer_name',
        'subdistrict_id',
        'address',
        'phone',
        'is_primary',
        'lat',
        'long',
        'icon'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class);
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
