<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    use HasFactory;

    protected $table = 'couriers';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'code',
        'icon_url',
        'usable',
    ];

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
