<?php

namespace App\Models\Promo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class VoucherClaim extends Model
{
    use HasFactory;

    protected $table = 'voucher_claims';

    protected $fillable = [
        'voucher_id',
        'user_id',
        'total_claim',
        'total_used'
    ];

    public function voucher()
    {
        return $this->belongsTo(Voucher::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
