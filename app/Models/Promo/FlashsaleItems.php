<?php

namespace App\Models\Promo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Sku;

class FlashsaleItems extends Model
{
    use HasFactory;

    protected $table = 'flashsale_items';

    protected $fillable = [
        'flashsale_id',
        'sku_id',
        'discount',
    ];

    public function flashsale()
    {
        return $this->belongsTo(Flashsale::class);
    }

    public function sku()
    {
        return $this->belongsTo(Sku::class)->withTrashed();
    }
}
