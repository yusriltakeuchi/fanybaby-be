<?php

namespace App\Models\Promo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Order\Order;

class Voucher extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'voucher';
    
    protected $fillable = [
        'name',
        'value',
        'type',
        'max_claim_total',
        'max_claim_per_user',
        'type_of_use',
        'min_total',
        'max_discount',
        'start_date',
        'end_date'
    ];

    public function voucherClaim()
    {
        return $this->hasMany(VoucherClaim::class);
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
