<?php

namespace App\Models\Promo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Product;

class Flashsale extends Model
{
    use HasFactory;

    protected $table = 'flashsale';

    protected $fillable = [
        'name',
        'image',
        'start',
        'end',
    ];

    public function scopeIsActive($query){
        return $query->where('start', '<=', now())->where('end', '>=', now());
    }

    public function items()
    {
        return $this->hasMany(FlashsaleItems::class);
    }
}
