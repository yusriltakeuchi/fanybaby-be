<?php

namespace App\Models\Misc;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $table = 'notification';

    protected $fillable = [
        'title',
        'body',
        'is_whatsapp',
        'is_push_notification',
        'notifiable_id',
        'notifiable_type'
    ];
}
