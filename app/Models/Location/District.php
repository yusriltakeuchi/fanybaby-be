<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;

    protected $table = 'area_districts';

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function subdistrict(){
        return $this->hasMany(Subdistrict::class);
    }

    public function province(){
        return $this->belongsTo(Province::class);
    }
}
