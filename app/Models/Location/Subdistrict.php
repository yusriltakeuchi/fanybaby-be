<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Order\Customer;

class Subdistrict extends Model
{
    use HasFactory;

    protected $table = 'area_subdistricts';

    public function district(){
        return $this->belongsTo(District::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function province(){
        return $this->belongsTo(Province::class);
    }

    public function customers(){
        return $this->hasMany(Customer::class);
    }
}
