<?php

namespace App\Exports\Order;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrderExport implements FromQuery, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $data;

    public function __construct($data){
        $this->data = $data;
    }

    public function query(){
        return $this->data;
    }

    public function map($order):array{
        return [
            'id' => $order->id,
            'created_at' => $order->created_at,
            'customer_name' => $order->customer->customer_name,
            'status' => $order->status,
            'courier' => $order->courier->name,
            'awb' => $order->awb ?? '-',
            'payment_method' => $order->orderPayment->payment_method,
            'subtotal' => $order->subtotal,
            'shipping_cost' => $order->shipping_fee,
            'voucher' => $order->voucher->name ?? '-',
            'total' => $order->total + $order->shipping_fee,
        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'Tanggal Dibuat',
            'Nama Pelanggan',
            'Status',
            'Kurir',
            'Resi',
            'Metode Pembayaran',
            'Subtotal',
            'Ongkos Kirim',
            'Voucher',
            'Total'
        ];
    }
}
