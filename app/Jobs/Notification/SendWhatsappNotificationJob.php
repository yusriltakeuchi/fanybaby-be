<?php

namespace App\Jobs\Notification;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Services\WAServices\NotifWA;
use App\Models\Misc\Notification;


class SendWhatsappNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $title;
    protected $body;
    protected $phone;
    /**
     * Create a new job instance.
     */
    public function __construct(Notification $notification, string $phone)
    {
        $this->title = $notification->title;
        $this->body = $notification->body;
        $this->phone = $phone;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $notifWA = new NotifWA();
        $notifWA->SendWhatsappNotification($this->title, $this->body, $this->phone);
    }
}
