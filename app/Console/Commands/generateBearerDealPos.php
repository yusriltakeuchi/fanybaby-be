<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class generateBearerDealPos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:generate-bearer-deal-pos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $url = 'https://fanybaby.dealpos.net/api/V3/Token';
        $client = new Client();
        $response = $client->post($url, [
            RequestOptions::JSON => [
                'client_id' => 'apps',
                'client_secret' => '888888'
            ]
        ]);
        $response = json_decode($response->getBody()->getContents());
        $token = $response->access_token;
        echo $token;
        $path = base_path('.env');
        $env = file_get_contents($path);

        if (file_exists($path)) {
            file_put_contents($path, str_replace('DEALPOS_TOKEN='.env('DEALPOS_TOKEN'), 'DEALPOS_TOKEN='.$token, $env));
        }
    }
}
