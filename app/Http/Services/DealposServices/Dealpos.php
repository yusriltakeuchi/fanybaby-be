<?php

namespace App\Http\Services\DealposServices;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use App\Models\User;
use Carbon\Carbon;

class Dealpos
{
    public function checkForRegisteredEmail(User $user)
    {
        $url = 'https://fanybaby.dealpos.net/api/V3/Customer/Email';
        $client = new Client();
        try {
            $response = $client->get($url, [
                RequestOptions::JSON => [
                    'Email' => $user->email ?? 'notfound@fanybaby.com'
                ],
                'headers' => [
                    'Authorization' => 'Bearer '.env('DEALPOS_TOKEN')
                ]
            ]);
            $response = json_decode($response->getBody()->getContents());
            return $this->syncData($response, $user);
        } catch(\Exception $e) {
            if($e->getCode() == 400) {
                return $this->checkForRegisteredPhone($user);
            } else {
                return response()->json([
                    'message' => 'Error',
                    'status' => 'error',
                    'data' => $e->getMessage()
                ]);
            }
        }
    }

    private function checkForRegisteredPhone(User $user)
    {
        if($user->phone_verified_at === null) {
            return $this->createNewCustomer($user);
        }
        $url = 'https://fanybaby.dealpos.net/api/V3/Customer/MobilePhone';
        $client = new Client();
        try {
            $response = $client->get($url, [
                RequestOptions::JSON => [
                    'MobilePhone' => $user->phone ?? '0'
                ],
                'headers' => [
                    'Authorization' => 'Bearer '.env('DEALPOS_TOKEN')
                ]
            ]);
            $response = json_decode($response->getBody()->getContents());
            return $this->syncData($response, $user);
        } catch(\Exception $e) {
            if($e->getCode() == 400) {
                return $this->createNewCustomer($user);
            } else {
                return response()->json([
                    'message' => 'Error',
                    'status' => 'error',
                    'data' => $e->getMessage()
                ]);
            }
        }
    }

    private function createNewCustomer(User $user)
    {
        $url = 'https://fanybaby.dealpos.net/api/V3/Customer';
        $client = new Client();
        $response = $client->post($url, [
            RequestOptions::JSON => [
                'Name' => $user->name,
                'Email' => $user->email,
                'Mobile' => $user->phone ?? null,
                'Code' => 'Apps-'.$user->id,
                'Status' => 'Active',
                'Birthdate' => $user->dob ?? ''
            ],
            'headers' => [
                'Authorization' => 'Bearer '.env('DEALPOS_TOKEN')
            ]
        ]);
        $response = json_decode($response->getBody()->getContents());
        return true;
    }

    private function syncData($response, User $user)
    {
        $user->name = $response->Name ?? $user->name;
        $user->email = $response->Email ?? $user->email;
        $user->phone = $response->MobilePhone ?? ($user->phone ?? null);
        $user->dob = $response->Birthday ? Carbon::parse($response->Birthday)->format('Y-m-d') : ($user->dob ?? null);
        $user->avatar = $response->ImageURL ?? ($user->avatar ?? null);
        $user->save();
        return true;
    }

    public function getPoint(User $user)
    {
        $url = 'https://fanybaby.dealpos.net/api/V3/LoyaltyPoint/EmailOrPhone';
        $client = new Client();
        try {
            $response = $client->get($url, [
                RequestOptions::JSON => [
                    'Email' => $user->email ?? '',
                    'MobilePhone' => $user->phone ?? '',
                    'PageNumber' => 1,
                    'PageSize' => 1
                ],
                'headers' => [
                    'Authorization' => 'Bearer '.env('DEALPOS_TOKEN')
                ]
            ]);
            $response = json_decode($response->getBody()->getContents());
            return $response[0]->PointsNow;
        } catch(\Exception $e) {
            return 0;
        }
    }

    public function addLoyaltyPoint(User $user)
    {
        $url = 'https://fanybaby.dealpos.net/api/V3/LoyaltyPoint';
        $client = new Client();
        try {
            $response = $client->put($url, [
                RequestOptions::JSON => [
                    'Email' => $user->email,
                    'LoyaltyPoint' => 100,
                    'Note' => 'Poin dari pembelian aplikasi',
                ],
                'headers' => [
                    'Authorization' => 'Bearer '.env('DEALPOS_TOKEN')
                ]
                ]);
            return true;
        } catch(\Exception $e) {
            return false;
        }
    }
}
