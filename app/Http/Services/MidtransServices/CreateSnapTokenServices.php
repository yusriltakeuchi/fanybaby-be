<?php

namespace App\Http\Services\MidtransServices;

use Midtrans\Snap;
use App\Models\Product\Sku;

class CreateSnapTokenServices extends MidtransServices
{
    public function getSnapToken($payment, $items, $customer)
    {
        $total_item = [];
        $totalPrice = 0;
        foreach ($items as $item) {
            $sku = Sku::find($item['id']);
            $totalPrice += ($sku->price * intval($item['quantity']));
            $total_item[] = [
                'id' => $sku->id,
                'price' => $sku->price,
                'quantity' => intval($item['quantity']),
                'name' => $sku->product->name,
            ];
        }
        $shipping_fee = $payment->total - $totalPrice;
        $total_item[] = [
            'id' => 'shipping_fee',
            'price' => $shipping_fee,
            'quantity' => 1,
            'name' => 'Biaya Pengiriman',
        ];
        $params = [
            'transaction_details' => [
                'order_id' => $payment->order_id,
                'gross_amount' => $payment->total,
            ],
            'item_details' => $total_item,
            'customer_details' => [
                'first_name' => $customer->customer_name,
                'phone' => auth()->user()->phone,
            ]
        ];
        return Snap::getSnapToken($params);
    }
}
