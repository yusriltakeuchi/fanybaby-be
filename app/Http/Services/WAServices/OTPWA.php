<?php

namespace App\Http\Services\WAServices;

use GuzzleHttp\Client;
use App\Models\Auth\OTP;
use App\Models\User;

class OTPWA
{
    private function sendMessage(string $phone, string $message): bool
    {
        $client = new Client([
            'base_uri' => 'https://whatsapp.idejualan.com/',
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);
        try {
            $response = $client->request('POST', 'send-message', [
                'form_params' => [
                    'api_key' => env('WA_API_KEY'),
                    'sender' => env('WA_SENDER'),
                    'number' => $phone,
                    'message' => $message,
                ],
            ]);
        }catch (\Exception $e) {
            return false;
        }
        if ($response->getStatusCode() == 200) {
            return true;
        } else {
            return false;
        }
    }

    public function sendOTP(string $phone, int $user_id, bool $resend = false): bool
    {
        $otp = $this->saveToOTP($phone, $user_id, $resend);
        $message = $this->sendMessage($phone, "Kode OTP FanyBaby anda adalah: ".$otp.", JANGAN BERIKAN KODE INI KEPADA SIAPAPUN, Terimakasih.");
        if ($message) {
            return true;
        } else {
            return false;
        }
    }

    private function saveToOTP(string $phone, int $user_id, bool $resend = false): int
    {
        if($resend == true){
            $otp = OTP::where('user_id', $user_id)->whereNull('verified_at')->orderBy('created_at', 'desc')->first();
            if($otp){
                $otp->delete();
            }
        }
        $otp = OTP::create([
            'user_id' => $user_id,
            'phone' => $phone,
            'code' => rand(100000, 999999),
            'expires_at' => now()->addMinutes(5),
        ]);
        return $otp->code;
    }

    public function sendForgotOTP($phone): bool
    {
        $user = User::where('phone', $phone)->first();
        $otp = $this->saveToOTP($phone, $user->id);
        $message = "Kode OTP FanyBaby untuk reset password anda adalah: ".$otp.", JANGAN BERIKAN KODE INI KEPADA SIAPAPUN, Terimakasih.";
        if ($this->sendMessage($phone, $message)) {
            return true;
        } else {
            return false;
        }
    }
}
