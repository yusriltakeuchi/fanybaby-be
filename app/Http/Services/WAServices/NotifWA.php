<?php

namespace App\Http\Services\WAServices;

use GuzzleHttp\Client;
use App\Models\Misc\Notification;

class NotifWA
{
    public function SendWhatsappNotification(string $title, string $body, string $phone){
        sleep(10);
        $client = new Client([
            'base_uri' => 'https://whatsapp.idejualan.com/',
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);
        try {
            $response = $client->request('POST', 'send-message', [
                'form_params' => [
                    'api_key' => env('WA_API_KEY'),
                    'sender' => env('WA_SENDER'),
                    'number' => $phone,
                    'message' => $title . PHP_EOL . $body,
                ],
            ]);
        }catch (\Exception $e) {
            return false;
        }
        if ($response->getStatusCode() == 200) {
            return true;
        } else {
            return false;
        }
    }
}