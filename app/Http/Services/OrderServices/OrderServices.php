<?php

namespace App\Http\Services\OrderServices;

use App\Models\Order\Order;
use App\Models\Order\OrderHistory;
use App\Models\Order\OrderItems;
use App\Models\Order\OrderPayment;
use App\Models\Promo\Voucher;
use App\Models\Promo\VoucherClaim;
use App\Models\Product\Sku;
use App\Models\Order\Courier;
use App\Models\Order\Customer;
use App\Models\Order\Cart;
use App\Http\Services\MidtransServices\CreateSnapTokenServices;

class OrderServices
{
    protected $subtotal = 0;
    protected $total = 0;

    public function place($data)
    {
        foreach ($data['sku'] as $sku_data) {
            $sku = Sku::find($sku_data['id']);
            if($sku->stock < $sku_data['quantity']) {
                return response()->json([
                    'message' => 'Stock not enough',
                    'status' => 'error'
                ], 400);
            }
            $this->subtotal += ($sku->price * $sku_data['quantity']);
        }
        return $this->checkForVoucher($data);
    }

    private function checkForVoucher($data)
    {
        if(isset($data['voucher_id'])) {
            $voucher = Voucher::where('id',$data['voucher_id'])->first();
            if($voucher) {
                $voucher_claim = VoucherClaim::where('voucher_id', $voucher->id)->where('user_id', auth()->user()->id)->first();
                if($voucher_claim) {
                    if($voucher->min_total > $this->subtotal) {
                        return response()->json([
                            'message' => 'Minimum total not reached',
                            'status' => 'error'
                        ], 400);
                    }
                    if($voucher_claim->total_claim <= $voucher->max_claim_per_user && $voucher_claim->total_used < $voucher->max_claim_per_user) {
                        $this->handleVoucherRedeem($voucher_claim);
                        return $this->createOrder($data, $voucher);
                    } else {
                        return response()->json([
                            'message' => 'Voucher has been fully used or claimed',
                            'status' => 'error'
                        ], 400);
                    }
                } else {
                    return $this->createOrder($data);
                }
            } else {
                return response()->json([
                    'message' => 'Voucher not found',
                    'status' => 'error'
                ], 404);
            }
        } else {
            return $this->createOrder($data);
        }
    }

    private function handleVoucherRedeem($voucher_claim)
    {
        $voucher_claim->update([
            'total_used' => $voucher_claim->total_used + 1,
        ]);
    }

    private function createOrder($data, $voucher = null)
    {
        if($voucher) {
            $discountVoucher = $voucher->type == 'percent' ? ($voucher->value / 100) * $this->subtotal : $voucher->value;
            if($discountVoucher > $voucher->max_discount){
                $discountVoucher = $voucher->max_discount;
            }
            $this->total = $this->subtotal - $discountVoucher;
        } else {
            $this->total = $this->subtotal;
        }
        $courier = Courier::where('code', $data['courier_code'])->first();
        $order = Order::create([
            'user_id' => auth()->user()->id,
            'voucher_id' => $voucher->id ?? null,
            'subtotal' => $this->subtotal,
            'total' => $this->total,
            'courier_id' => $courier->id,
            'courier_services' => $data['courier_service'],
            'shipping_fee' => $data['courier_cost'],
            'customer_id' => $data['customer_id'],
            'is_cod' => $data['is_cod'] ?? false,
        ]);
        $this->createOrderItems($data, $order);
        $this->createOrderHistory($order);
        $this->reduceStock($data);
        $this->removeCartUser($data);
        $snap = $this->createOrderPayment($data, $order);
        return $snap;
    }

    private function reduceStock($data)
    {
        foreach ($data['sku'] as $sku_data) {
            $sku = Sku::find($sku_data['id']);
            $sku->update([
                'stock' => $sku->stock - $sku_data['quantity']
            ]);
        }
    }

    private function removeCartUser($data)
    {
        foreach ($data['sku'] as $sku_data) {
            $cart = Cart::where('user_id', auth()->user()->id)->where('sku_id', $sku_data['id'])->first();
            if($cart->quantity <= $sku_data['quantity']) {
                $cart->delete();
            } else {
                $cart->update([
                    'quantity' => $cart->quantity - $sku_data['quantity']
                ]);
            }
        }
    }

    private function createOrderItems($data, $order)
    {
        foreach ($data['sku'] as $sku_data) {
            OrderItems::create([
                'order_id' => $order->id,
                'sku_id' => $sku_data['id'],
                'quantity' => $sku_data['quantity']
            ]);
        }
    }

    private function createOrderHistory($order)
    {
        OrderHistory::create([
            'order_id' => $order->id,
            'status' => $order->is_cod ? 'paid' : 'pending',
        ]);
    }

    private function createOrderPayment($data, $order)
    {
        $payment = OrderPayment::create([
            'order_id' => $order->id,
            'total' => $order->subtotal + $order->shipping_fee,
            'payment_method' => isset($data['is_cod']) ? 'COD' : 'midtrans',
            'payment_status' => isset($data['is_cod']) ? 'paid' : 'pending',
        ]);
        $customer = Customer::find($data['customer_id']);
        $snap_token = isset($data['is_cod']) ? 'false' : (new CreateSnapTokenServices())->getSnapToken($payment, $data['sku'], $customer);
        $payment->update([
            'midtrans_snap_token' => $snap_token
        ]);
        return response()->json([
            'status' => 'success',
            'data' => [
                'snap_token' => $snap_token
            ],
            'message' => isset($data['is_cod']) ? 'Order created' : 'Order created, please complete payment'
        ]);
    }
}
