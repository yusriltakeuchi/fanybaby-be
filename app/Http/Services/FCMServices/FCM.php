<?php

namespace App\Http\Services\FCMServices;

use App\Models\Misc\Notification;
use App\Models\User;

class Fcm
{
    public function sendNotification($title, $body, $recipients = null)
    {
        $notifData = [
            'title' => $title,
            'body' => $body,
            'sound' => 'default',
            'android_channel_id' => 'high_importance_channel',
        ];
        $data['click_action'] = 'FLUTTER_NOTIFICATION_CLICK';
        $data['title'] = $title;
        $data['body'] = $body;

        if($recipients == null) {
            fcm()
            ->toTopic('fanybaby')
            ->priority('high')
            ->timeToLive(0)
            ->notification($notifData)
            ->data($data)
            ->send();
        } else {
            fcm()
            ->to($recipients)
            ->priority('high')
            ->timeToLive(0)
            ->notification($notifData)
            ->data($data)
            ->send();
        }
    }
}
