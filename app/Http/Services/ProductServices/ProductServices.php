<?php

namespace App\Http\Services\ProductServices;

use App\Models\Product\Product;
use App\Models\Product\Sku;

class ProductServices
{
    public function store($data)
    {
        $data->image = $data['image']->store('products', 'public');
        $product = Product::create([
            'name' => $data->name,
            'total_rating' => 0,
            'rating' => 0,
            'description' => $data->description,
            'category_id' => $data->category_id,
            'image' => $data->image,
            'brand_id' => $data->brand_id ?? '',
        ]);
        $skus = [];
        foreach ($data->skus as $sku) {
            $sku['image'] = $sku['image']->store('products', 'public');
            $skus[] = new Sku([
                'code' => $sku['code'],
                'image' => $sku['image'],
                'price' => $sku['price'],
                'properties' => json_encode($sku['properties']),
                'stock' => $sku['stock'],
                'weight_gram' => $sku['weight_gram'] ?? 0,
            ]);
        }
        $product->skus()->saveMany($skus);
        return $product;
    }

    public function updateProduct($data, $product)
    {
        $data['image'] ? $data->image = $data['image']->store('product', 'public') : $data->image = $product->image;
        $product->update([
            'name' => $data->name,
            'description' => $data->description,
            'category_id' => $data->category_id,
            'image' => $data->image,
            'brand_id' => $data->brand_id ?? '',
        ]);
        $skus = [];
        foreach ($data->skus as $sku) {
            $sku_old = Sku::where('code', $sku['code'])->first();
            if ($sku_old) {
                if (array_key_exists('image', $sku)) {
                    $sku['image'] = $sku['image']->store('products', 'public');
                } else {
                    $sku['image'] = $sku_old->image;
                }
                $sku_old->update([
                    'code' => $sku['code'],
                    'image' => $sku['image'],
                    'price' => $sku['price'],
                    'properties' => json_encode($sku['properties']),
                    'stock' => $sku['stock'],
                    'weight_gram' => $sku['weight_gram'] ?? $sku_old->weight_gram,
                ]);
            } else {
                $sku['image'] = $sku['image']->store('products', 'public');
                $skus[] = new Sku([
                    'code' => $sku['code'],
                    'image' => $sku['image'],
                    'price' => $sku['price'],
                    'properties' => json_encode($sku['properties']),
                    'stock' => $sku['stock'],
                    'weight_gram' => $sku['weight_gram'] ?? 0,
                ]);
            }
        }
        $unusedSku = collect($data->skus)->pluck('code')->toArray();
        $sku_old = Sku::where('product_id', $product->id)->whereNotIn('code', $unusedSku)->get();
        $sku_old->each(function ($sku) {
            $sku->delete();
        });
        $skus != [] ? $product->skus()->saveMany($skus) : null;
        return $product;
    }
}
