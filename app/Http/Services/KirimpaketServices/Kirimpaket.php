<?php

namespace App\Http\Services\KirimpaketServices;

use App\Models\Location\Subdistrict;
use App\Models\Order\Order;
use GuzzleHttp\Client;

class Kirimpaket
{
    public function checkOngkir(Subdistrict $origin, Subdistrict $destination, int $weight)
    {
        $client = new Client();

        $response = $client->request('POST', env('KIRIMPAKET_PRODUCTION_ENDPOINT').'api/open/shipping/price-list-by-region', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.env('KIRIMPAKET_API_KEY'),
            ],
            'json' => [
                'origin' => [
                    'province' => $origin->province->name,
                    'city' => $origin->city->name,
                    'district' => $origin->district->name,
                    'subdistrict' => $origin->name,
                    'postal_code' => $origin->postal_code,
                ],
                'destination' => [
                    'province' => $destination->province->name,
                    'city' => $destination->city->name,
                    'district' => $destination->district->name,
                    'subdistrict' => $destination->name,
                    'postal_code' => $destination->postal_code,
                ],
                'weight' => $weight,
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getAwb($awb)
    {
        $client = new Client();
        $token = env('KIRIMPAKET_API_KEY');
        try {
            $response = $client->request('GET', 'https://api.kirimpaket.id/api/open/shipping/track-airwaybill', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer '.$token,
                ],
                'query' => [
                    'awb_number' => $awb,
                ],
            ]);

            $body = $response->getBody();
            $data = json_decode($body);

            return response()->json([
                'status' => 'success',
                'data' => [
                    'awb_number' => $data->data->awb_number,
                    'status' => $data->data->status,
                    'history' => $data->data->history,
                ],
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'awb tidak ditemukan',
            ], 400);
        }
    }

    public function createOrder(Order $order)
    {
        $client = new Client();
        $token = env('KIRIMPAKET_API_KEY');
        $url = env('KIRIMPAKET_PRODUCTION_ENDPOINT').'api/open/order/integration';
        // $url = env('KIRIMPAKET_DEVELOPMENT_ENDPOINT').'api/open/order/integration';
        $products = [];
        foreach ($order->orderItems as $item) {
            $products[] = [
                'name' => $item->sku->product->name,
                'sku' => $item->sku->code,
                'qty' => $item->quantity,
                'price' => $item->sku->price,
                'weight_gram' => $item->sku->weight_gram,
            ];
        }
        try {
            $response = $client->post($url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer '.$token,
                ],
                'json' => [
                    'receiver' => [
                        'name' => $order->customer->customer_name,
                        'phone' => $order->customer->phone,
                        'email' => 'rafly@idejualan.com',
                        'province' => $order->customer->subdistrict->province->name,
                        'city' => $order->customer->subdistrict->city->name,
                        'district' => $order->customer->subdistrict->district->name,
                        'subdistrict' => $order->customer->subdistrict->name,
                        'postal_code' => $order->customer->subdistrict->postal_code,
                        'address' => $order->customer->address,
                    ],
                    'sender' => [
                        'name' => 'Fany Baby ITC Kuningan',
                        'phone' => '081389282323',
                        'province' => 'DKI JAKARTA',
                        'city' => 'KOTA ADM. JAKARTA SELATAN',
                        'district' => 'SETIA BUDI',
                        'subdistrict' => 'KARET KUNINGAN',
                        'postal_code' => 12940,
                        'address' => ' ITC Kuningan, Jl. Prof. DR. Satrio No.1, RT.11/RW.4, Kuningan,',
                    ],
                    'products' => $products,
                    'expedition' => [
                        'rate' => $order->shipping_fee,
                        'logistic_name' => $order->courier->name,
                        'rate_name' => $order->courier_services,
                        'min_day' => 1,
                        'max_day' => 3,
                        'service' => 'pickup',
                        'cod_amount' => $order->is_cod ? $order->total : 0,
                    ],
                    'payment' => [
                        'status' => 'paid',
                    ],
                ],
            ]);

            $body = $response->getBody();
            $data = json_decode($body);
            $order->update([
                'awb' => $data->data->awb_number,
            ]);

            return response()->json([
                'status' => 'success',
                'data' => $data->data,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 400);
        }
    }
}
