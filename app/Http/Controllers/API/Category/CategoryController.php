<?php

namespace App\Http\Controllers\API\Category;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Http\Requests\category\CategoryStoreRequest;
use App\Http\Requests\category\CategoryUpdateRequest;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Http\Resources\Category\IndexCategoryResource;

class CategoryController extends Controller
{
    /**
     * Store a newly created resource in storage.
     * @param CategoryStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(CategoryStoreRequest $request){
        $data = $request->validated();
        $data['icon'] = $request->file('icon')->store('category','public');
        $category = Category::create($data);

        return response()->json([
            'message' => 'Category created successfully',
            'status' => 'success',
            'data' => $category,
        ], 201);
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Spatie\QueryBuilder\Exceptions\InvalidFilterQuery
     */
    public function index(){
        $categories = QueryBuilder::for(Category::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('name'),
                AllowedFilter::exact('is_featured'),
                AllowedFilter::exact('is_parent'),
                AllowedFilter::exact('parent_id'),
            ])
            ->allowedSorts([
                'id',
                'name',
            ])
            ->defaultSort('-id')
            ->where('is_parent', 1)
            ->paginate(Request('limit') ?? 20);

        return response()->json([
            'message' => 'Category list',
            'status' => 'success',
            'data' => IndexCategoryResource::collection($categories),
            'pagination' => [
                'total' => $categories->total(),
                'per_page' => $categories->perPage(),
                'current_page' => $categories->currentPage(),
                'last_page' => $categories->lastPage(),
                'from' => $categories->firstItem(),
                'to' => $categories->lastItem(),
            ],
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     * @param CategoryUpdateRequest $request
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(CategoryUpdateRequest $request, Category $category){
        $data = $request->validated();
        $request->icon ? $data['icon'] = $request->file('icon')->store('category','public') : ($category->icon ? $data['icon'] = $category->icon : $data['icon'] = null);
        $category->update($data);

        return response()->json([
            'message' => 'Category updated successfully',
            'status' => 'success',
            'data' => $category,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Category $category){
        $category->delete();

        return response()->json([
            'message' => 'Category deleted successfully',
            'status' => 'success',
            'data' => $category,
        ], 200);
    }
}
