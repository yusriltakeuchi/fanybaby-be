<?php

namespace App\Http\Controllers\API\Misc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\Order\OrderExport;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Models\Order\Order;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function exportOrder(){
        $data = QueryBuilder::for(Order::class)
        ->with(['orderItems','orderHistory'])
        ->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('status'),
        ])
        ->allowedSorts([
            'id',
            'status'
        ])
        ->defaultSort('-id');

        return Excel::download(new OrderExport($data), 'order-'.now().'.xlsx');
    }
}
