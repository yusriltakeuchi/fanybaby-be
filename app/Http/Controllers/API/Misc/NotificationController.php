<?php

namespace App\Http\Controllers\API\Misc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Misc\Notification;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Http\Requests\misc\CreateNotificationRequest;
use App\Http\Resources\Notification\IndexNotificationResource;
use App\Jobs\Notification\SendWhatsappNotificationJob;
use App\Http\Services\FCMServices\FCM;
use App\Models\User;


class NotificationController extends Controller
{
    public function index()
    {
        $notifications = QueryBuilder::for(Notification::class)
            ->allowedFilters([
                AllowedFilter::partial('title'),
                AllowedFilter::exact('is_whatsapp'),
                AllowedFilter::exact('is_push_notification'),
                AllowedFilter::exact('notifiable_id'),
                AllowedFilter::partial('notifiable_type'),
            ])
            ->defaultSort('-created_at')
            ->paginate(Request('limit') ?? 10);
        return response()->json(
            [
                'message' => 'success',
                'data' => IndexNotificationResource::collection($notifications),
                'pagination' => [
                    'current_page' => $notifications->currentPage(),
                    'last_page' => $notifications->lastPage(),
                    'per_page' => $notifications->perPage(),
                    'total' => $notifications->total(),
                ],
            ]
        );
    }

    public function store(CreateNotificationRequest $request){
        $notification = Notification::create($request->validated());
        if($request->is_whatsapp){
            User::whereNotNull('phone_verified_at')->chunk(1, function ($users) use ($notification) {
                $iteration = 1;
                foreach ($users as $user) {
                    $delaySecond = $iteration * 5;
                    $dispatcher = (new SendWhatsappNotificationJob($notification, $user->phone))->delay(now()->addSeconds($delaySecond));
                    dispatch($dispatcher);
                }
            });
        }
        if($request->is_push_notification){
            $fcm = new FCM();
            $fcm->sendNotification($notification->title, $notification->body);
        }
        return response()->json(
            [
                'message' => 'success',
                'data' => new IndexNotificationResource($notification),
            ]
        );
    }

    public function update(CreateNotificationRequest $request, Notification $notification){
        $notification->update($request->validated());
        return response()->json(
            [
                'message' => 'success',
                'data' => new IndexNotificationResource($notification),
            ]
        );
    }

    public function destroy(Notification $notification){
        $notification->delete();
        return response()->json(
            [
                'message' => 'success',
            ]
        );
    }
}
