<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Models\User;
use App\Http\Resources\User\IndexUserResource;
use App\Http\Requests\user\CreateUserRequest;
use App\Http\Requests\user\UpdateUserRequest;

class UserController extends Controller
{
    public function index(){
        $data = QueryBuilder::for(User::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('name'),
                AllowedFilter::partial('email'),
                AllowedFilter::partial('phone')
            ])
            ->allowedSorts([
                'id',
                'name'
            ])
            ->defaultSort('-id')
            ->paginate(request('limit') ?? 20);

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menampilkan data',
            'data' => IndexUserResource::collection($data),
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastItem()
            ]
        ]);
    }

    public function store(CreateUserRequest $request){
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'phone_verified_at' => now(),
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menambahkan data',
            'data' => $user
        ]);
    }

    public function update(CreateUserRequest $request, User $user){
        $user->update([
            'name' => $request->name,
            'email' => $request->email ?? $user->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password) ?? $user->password,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil mengubah data',
            'data' => $user
        ]);
    }

    public function destroy(User $user){
        $user->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menghapus data',
        ]);
    }

    public function updateUser(UpdateUserRequest $request){
        $user = auth()->user();
        $data = $request->validated();
        $avatar = $data['avatar'] ? $request->file('avatar')->store('user', 'public') : ($user->avatar ?? null);
        $user->update([
            'name' => $data['name'],
            'avatar' => $avatar,
            'dob' => $data['dob']
        ]);
        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil mengubah data',
            'data' => $user
        ]);
    }

    public function updatePassword(){
        $user = auth()->user();
        if (\Hash::check(request('old_password'), $user->password)) {
            $user->password = bcrypt(request('new_password'));
            $user->save();
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Password lama salah',
            ]);
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil mengubah password',
            'data' => $user
        ]);
    }

    public function updatePhone(Request $request){
        $user = auth()->user();
        $user->update([
            'phone' => $request->phone
        ]);
        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil mengubah nomor telepon, silahkan verifikasi nomor telepon anda',
            'data' => $user
        ]);
    }
}
