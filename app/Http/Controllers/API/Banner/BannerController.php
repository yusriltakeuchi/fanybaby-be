<?php

namespace App\Http\Controllers\API\Banner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner\Banner;
use App\Http\Requests\banner\BannerStoreRequest;
use App\Http\Requests\banner\BannerUpdateRequest;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Http\Resources\Banner\IndexBannerResource;

class BannerController extends Controller
{
    /**
     * Store a newly created resource in storage.
     * @param BannerStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(BannerStoreRequest $request){
        $data = $request->validated();
        $data['image'] = $request->file('image')->store('banner','public');
        $banner = Banner::create($data);

        return response()->json([
            'message' => 'Banner created successfully',
            'data' => $banner,
        ], 201);
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Spatie\QueryBuilder\Exceptions\InvalidFilterQuery
     */
    public function index(){
        $banners = QueryBuilder::for(Banner::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('name'),
                AllowedFilter::exact('type')
            ])
            ->allowedSorts([
                'id',
                'name'
            ])
            ->defaultSort('-id')
            ->paginate(Request('limit') ?? 20);

        return response()->json([
            'message' => 'Banner list',
            'data' => IndexBannerResource::collection($banners),
            'pagination' => [
                'total' => $banners->total(),
                'per_page' => $banners->perPage(),
                'current_page' => $banners->currentPage(),
                'last_page' => $banners->lastPage(),
                'from' => $banners->firstItem(),
                'to' => $banners->lastItem()
            ]
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     * @param BannerUpdateRequest $request
     * @param Banner $banner
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Banner $banner, BannerUpdateRequest $request){
        $data = $request->validated();
        $request->image ? $data['image'] = $request->file('image')->store('banner','public') : ($banner->image ? $data['image'] = $banner->image : $data['image'] = null);
        $banner->update($data);

        return response()->json([
            'message' => 'Banner updated successfully',
            'data' => $banner,
            'pagination' => [
                'total' => $banner->total(),
                'per_page' => $banner->perPage(),
                'current_page' => $banner->currentPage(),
                'last_page' => $banner->lastPage(),
                'from' => $banner->firstItem(),
                'to' => $banner->lastItem()
            ]
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param Banner $banner
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Banner $banner){
        $banner->delete();

        return response()->json([
            'message' => 'Banner deleted successfully',
            'data' => $banner,
        ], 200);
    }
}
