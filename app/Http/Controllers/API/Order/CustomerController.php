<?php

namespace App\Http\Controllers\API\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order\Customer;
use App\Http\Requests\order\CustomerStoreRequest;
use App\Http\Resources\Order\IndexCustomerResources;

class CustomerController extends Controller
{
    public function addCustomer(CustomerStoreRequest $request){
        if($request->is_primary == 1){
            Customer::where('user_id', $request->user()->id)->update(['is_primary' => 0]);
        }
        $customer = Customer::create([
            'user_id' => $request->user()->id,
            'title' => $request->title,
            'customer_name' => $request->customer_name,
            'subdistrict_id' => $request->subdistrict_id,
            'address' => $request->address,
            'phone' => $request->phone,
            'is_primary' => $request->is_primary,
            'lat' => $request->lat ?? null,
            'long' => $request->long ?? null,
            'icon' => $request->icon ?? null,
        ]);
        return response()->json([
            'status' => 'success',
            'data' => $customer
        ]);
    }

    public function getMyAddress(){
        $customer = Customer::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->get();
        return response()->json([
            'status' => 'success',
            'data' => IndexCustomerResources::collection($customer)
        ]);
    }

    public function deleteAddress(Customer $customer){
        $customer->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'Address deleted'
        ]);
    }

    public function setPrimary(Customer $customer){
        Customer::where('user_id', auth()->user()->id)->update(['is_primary' => 0]);
        $customer->update(['is_primary' => 1]);
        return response()->json([
            'status' => 'success',
            'message' => 'Address set as primary'
        ]);
    }

    public function update(CustomerStoreRequest $request, Customer $customer){
        if($request->is_primary == 1){
            Customer::where('user_id', $request->user()->id)->update(['is_primary' => 0]);
        }
        $customer->update([
            'title' => $request->title,
            'customer_name' => $request->customer_name,
            'subdistrict_id' => $request->subdistrict_id,
            'address' => $request->address,
            'phone' => $request->phone,
            'is_primary' => $request->is_primary,
            'lat' => $request->lat ?? null,
            'long' => $request->long ?? null,
            'icon' => $request->icon ?? null,
        ]);
        return response()->json([
            'status' => 'success',
            'message' => 'Address updated'
        ]);
    }
}
