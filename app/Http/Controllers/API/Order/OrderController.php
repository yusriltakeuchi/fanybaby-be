<?php

namespace App\Http\Controllers\API\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\order\PlaceOrderRequest;
use App\Http\Services\OrderServices\OrderServices;
use App\Models\Order\OrderPayment;
use App\Models\Order\Order;
use App\Http\Resources\Order\IndexOrderResources;
use App\Http\Resources\Order\IndexOrderDetailResources;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Http\Services\KirimpaketServices\Kirimpaket;
use App\Http\Services\DealposServices\Dealpos;

class OrderController extends Controller
{
    public function place(PlaceOrderRequest $request, OrderServices $service)
    {
        $order = $service->place($request->validated());
        return $order;
    }

    public function myOrder()
    {
        $orders = QueryBuilder::for(Order::class)
            ->where('user_id', auth()->user()->id)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('status'),
            ])
            ->allowedSorts([
                'id',
                'status'
            ])
            ->defaultSort('-id')
            ->paginate(request('limit') ?? 20);
        return response()->json([
            'status' => 'success',
            'data' => IndexOrderResources::collection($orders)
        ]);
    }

    public function detailOrder(Order $order)
    {
        $order->load(['orderItems','orderHistory']);
        return response()->json([
            'status' => 'success',
            'data' => new IndexOrderDetailResources($order)
        ]);
    }

    public function cancelOrder(Order $order)
    {
        if($order->status == 'paid') {
            return response()->json([
                'status' => 'error',
                'message' => 'order sudah dibayar'
            ], 400);
        }
        $order->update([
            'status' => 'canceled'
        ]);
        $order->orderHistory()->create([
            'status' => 'canceled'
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'success cancel order'
        ]);
    }

    public function notification(Request $request)
    {
        $data = $request->all();
        $order_id = $data['order_id'];
        $order_payment = OrderPayment::where('order_id', $order_id)->first();
        $order_payment->update([
            'payment_method' => $data['payment_type'],
            'payment_status' => $data['transaction_status'] == 'settlement' || $data['transaction_status'] == 'capture' ? 'paid' : 'pending',
        ]);
        if($data['transaction_status'] == 'settlement' || $data['transaction_status'] == 'capture') {
            $order_payment->order->orderHistory()->create([
                'status' => 'paid',
            ]);
            $order_payment->order->update([
                'status' => 'paid',
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'success update payment status'
        ]);
    }

    public function getAwb(Request $request, Kirimpaket $services)
    {
        return $services->getAwb($request->awb_number);
    }

    public function completeOrder(Order $order, Dealpos $services){
        if($order->status != 'on_delivery') {
            return response()->json([
                'status' => 'error',
                'message' => 'order belum dikirim'
            ], 400);
        }
        $order->update([
            'status' => 'completed'
        ]);
        $order->orderHistory()->create([
            'status' => 'completed'
        ]);

        $loyalty = $services->addLoyaltyPoint($order->user);

        return response()->json([
            'status' => 'success',
            'message' => 'berhasil update status',
            'loyalty' => $loyalty
        ]);
    }
}
