<?php

namespace App\Http\Controllers\API\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Location\Province;
use App\Models\Location\City;
use App\Models\Location\District;
use App\Models\Location\Subdistrict;
use App\Http\Requests\order\CheckOngkirRequest;
use App\Http\Services\KirimpaketServices\Kirimpaket;


class OngkirController extends Controller
{
    public function province(){
        $provinces = Province::select('id','name')->get();
        return response()->json([
            'status' => 'success',
            'data' => $provinces
        ]);
    }

    public function city(Province $province){
        $cities = $province->cities()->select('id','name')->get();
        return response()->json([
            'status' => 'success',
            'data' => $cities
        ]);
    }

    public function district(City $city){
        $districts = $city->districts()->select('id','name')->get();
        return response()->json([
            'status' => 'success',
            'data' => $districts
        ]);
    }

    public function subdistrict(District $district){
        $subdistricts = $district->subdistrict()->select('id','name')->get();
        return response()->json([
            'status' => 'success',
            'data' => $subdistricts
        ]);
    }

    public function checkOngkir(CheckOngkirRequest $request, Kirimpaket $services){
        $origin = Subdistrict::find($request->origin);
        $destination = Subdistrict::find($request->destination);
        $data = $services->checkOngkir($origin, $destination, $request->weight);
        return response()->json([
            'status' => 'success',
            'data' => $data['data']
        ]);
    }
}
