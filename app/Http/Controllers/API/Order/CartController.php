<?php

namespace App\Http\Controllers\API\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\order\CartStoreRequest;
use App\Http\Resources\Order\IndexCartResources;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $carts = auth()->user()->cart;
        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menampilkan keranjang',
            'data' => IndexCartResources::collection($carts)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param CartStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function addToCart(CartStoreRequest $request){
        $cart = auth()->user()->cart()->where('sku_id', $request->sku_id)->first();
        if($cart){
            $cart->increment('quantity', $request->quantity);
        }else{
            $cart = auth()->user()->cart()->create([
                'sku_id' => $request->sku_id,
                'quantity' => $request->quantity,
                'ip_address' => request()->ip(),
            ]);
        }
        return response()->json([
            'message' => 'Berhasil menambahkan ke keranjang',
            'cart' => $cart
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param CartStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function removeFromCart(CartStoreRequest $request){
        $cart = auth()->user()->cart()->where('sku_id', $request->sku_id)->first();
        if(!$cart){
            return response()->json([
                'message' => 'Keranjang tidak ditemukan'
            ], 404);
        }
        if($cart->quantity >= 1){
            if($request->quantity < $cart->quantity){
                $cart->decrement('quantity', $request->quantity);
            }else{
                $cart->delete();
            }
        }
        return response()->json([
            'message' => 'Berhasil menghapus dari keranjang',
            'cart' => $cart
        ]);
    }
}
