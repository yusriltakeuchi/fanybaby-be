<?php

namespace App\Http\Controllers\API\Order;

use App\Http\Controllers\Controller;
use App\Http\Resources\Order\IndexOrderAdminResources;
use App\Http\Services\FCMServices\FCM;
use App\Http\Services\KirimpaketServices\Kirimpaket;
use App\Models\Order\Order;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OrderAdminController extends Controller
{
    public function index()
    {
        $data = QueryBuilder::for(Order::class)
            ->with(['orderItems', 'orderHistory'])
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::exact('status'),
            ])
            ->allowedSorts([
                'id',
                'status',
            ])
            ->defaultSort('-id')
            ->paginate(request('limit') ?? 10)
        ;

        return response()->json([
            'status' => 'success',
            'data' => IndexOrderAdminResources::collection($data),
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastItem(),
            ],
        ]);
    }

    public function updateToOnProcess(Kirimpaket $services, Order $order)
    {
        $order->update([
            'status' => 'on_process',
        ]);
        $order->orderHistory()->create([
            'status' => 'on_process',
        ]);
        if ((null != $order->user->phone_verified_at) && (null != $order->user->fcm_token)) {
            $fcm = new FCM();
            $fcm->sendNotification('Pemberitahuan Pesanan', 'Pesanan anda sedang diproses', [$order->user->fcm_token]);
        }

        return $services->createOrder($order);
    }

    public function updateToOnDelivery(Order $order)
    {
        $order->update([
            'status' => 'on_delivery',
        ]);
        $order->orderHistory()->create([
            'status' => 'on_delivery',
        ]);

        if ((null != $order->user->phone_verified_at) && (null != $order->user->fcm_token)) {
            $fcm = new FCM();
            $fcm->sendNotification('Pemberitahuan Pesanan', 'Pesanan anda sedang dikirim oleh kurir', [$order->user->fcm_token]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'berhasil update status',
        ]);
    }

    public function updateToComplete(Order $order)
    {
        $order->update([
            'status' => 'completed',
        ]);
        $order->orderHistory()->create([
            'status' => 'completed',
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'berhasil update status',
        ]);
    }

    public function updateToDownloaded(Request $request)
    {
        $request->validate([
            'order_id' => 'required|array',
            'order_id.*' => 'required|exists:orders,id',
        ]);

        Order::whereIn('id', $request->get('order_id'))->update([
            'is_downloaded' => true,
        ]);
    }

    public function updateToCancel(Order $order)
    {
        if (!in_array($order->status, ['pending', 'paid'])) {
            return response()->json([
                'status' => 'error',
                'message' => 'you can only cancel order with status pending or paid',
            ], 400);
        }

        $order->update([
            'status' => 'canceled',
        ]);
        if ((null != $order->user->phone_verified_at) && (null != $order->user->fcm_token)) {
            $fcm = new FCM();
            $fcm->sendNotification('Pemberitahuan Pesanan', 'Pesanan anda dibatalkan oleh Admin', [$order->user->fcm_token]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'berhasil update status',
        ]);
    }
}
