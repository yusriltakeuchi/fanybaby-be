<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MiscController extends Controller
{
    public function version()
    {
        return response()->json([
                'data' => [
                    'android' => [
                        'version' => env('FANYBABY_APPS_ANDROID_VERSION'),
                        'version_code' => env('FANYBABY_APPS_ANDROID_VERSION_CODE'),
                        'force_update' => env('FANYBABY_APPS_ANDROID_FORCE_UPDATE'),
                    ]
                ],
                'message' => 'success'
            ]);
    }
}
