<?php

namespace App\Http\Controllers\API\Analytic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Order\Order;
use App\Models\Product\Product;
use App\Models\Order\OrderItems;
use Carbon\Carbon;
use DB;

class AnalyticAdminController extends Controller
{
    public function analyticDashboard()
    {
        $totalUser = User::count();
        $totalOrder = Order::count();
        $totalProduct = Product::count();
        $totalSold = intval(OrderItems::sum('quantity'));
        $totalSoldInMonth = intval(OrderItems::where('created_at', '>=', Carbon::now()->startOfMonth())->sum('quantity'));
        $totalOrderInMonth = Order::where('created_at', '>=', Carbon::now()->startOfMonth())->where('status', '!=', 'unpaid')->where('status', '!=', 'canceled')->count();
        $totalDayFromFirstMonth = Carbon::now()->startOfMonth()->diffInDays(Carbon::now());
        $totalSoldPerDay = $totalSoldInMonth != 0 ? round($totalSoldInMonth / $totalDayFromFirstMonth, 3) : 0;
        $totalOrderPerDay = $totalOrderInMonth != 0 ? round($totalOrderInMonth / $totalDayFromFirstMonth, 3) : 0;
        $topProduct = DB::table('order_items')
        ->select('p.name', DB::raw('SUM(order_items.quantity) as total_sold'))
        ->leftJoin('skus AS s', 'order_items.sku_id', '=', 's.id')
        ->leftJoin('products AS p', 's.product_id', '=', 'p.id')
        ->groupBy('p.name')
        ->orderByRaw('SUM(order_items.quantity) DESC')
        ->limit(10)
        ->get();
        $topCategory = DB::table('order_items')
        ->select('s.id', 'p.id', 'c.id', 'c.name', DB::raw('COUNT(*) as total_sold'))
        ->leftJoin('skus AS s', 'order_items.sku_id', '=', 's.id')
        ->leftJoin('products AS p', 's.product_id', '=', 'p.id')
        ->leftJoin('category AS c', 'p.category_id', '=', 'c.id')
        ->where('order_items.created_at', '>=', Carbon::now()->startOfMonth())
        ->groupBy('s.id', 'p.id', 'c.id', 'c.name')
        ->orderByRaw('COUNT(*) DESC')
        ->limit(10)
        ->get();
        $newestUser = User::select('id','name','avatar', 'created_at')->orderBy('created_at', 'DESC')->limit(10)->get();
        $newestOrder = DB::table('orders')
        ->select('orders.id', 'orders.total', 'users.name', 'couriers.name as courier_name')
        ->leftJoin('users', 'orders.user_id', '=', 'users.id')
        ->leftJoin('couriers', 'orders.courier_id', '=', 'couriers.id')
        ->orderBy('orders.created_at', 'DESC')
        ->limit(10)
        ->get();
        $unpaidOrder = DB::table('orders')
        ->select('orders.id', 'orders.total', 'users.name', 'couriers.name as courier_name')
        ->leftJoin('users', 'orders.user_id', '=', 'users.id')
        ->leftJoin('couriers', 'orders.courier_id', '=', 'couriers.id')
        ->where('orders.status', '=', 'pending')
        ->orderBy('orders.created_at', 'DESC')
        ->limit(10)
        ->get();
        $topCity = DB::table('orders')
        ->select('area_cities.id', 'area_cities.name', DB::raw('COUNT(*) as total_order'))
        ->leftJoin('customer', 'orders.customer_id', '=', 'customer.id')
        ->leftJoin('area_subdistricts', 'customer.subdistrict_id', '=', 'area_subdistricts.id')
        ->leftJoin('area_cities', 'area_subdistricts.city_id', '=', 'area_cities.id')
        ->groupBy('area_cities.id', 'area_cities.name')
        ->orderByRaw('COUNT(*) DESC')
        ->limit(10)
        ->get();
        $topUser = DB::table('orders')
        ->select('users.id', 'users.name', 'users.avatar', DB::raw('COUNT(*) as total_order'))
        ->leftJoin('users', 'orders.user_id', '=', 'users.id')
        ->groupBy('users.id', 'users.name', 'users.avatar')
        ->orderByRaw('COUNT(*) DESC')
        ->limit(10)
        ->get();
        $userAge = User::whereNotNull('dob')->select(DB::raw('TIMESTAMPDIFF(YEAR, dob, CURDATE()) AS age'))->get();
        $firstAge = $userAge->whereBetween('age', [20, 25])->count();
        $secondAge = $userAge->whereBetween('age', [26, 35])->count();
        $thirdAge = $userAge->whereBetween('age', [36, 50])->count();
        $fourthAge = $userAge->where('age', '>', 50)->count();

        return response()->json([
            'countable' => [
                'total_user' => $totalUser,
                'total_order' => $totalOrder,
                'total_product' => $totalProduct,
                'total_sold' => $totalSold,
                'average_sold_per_day' => $totalSoldPerDay,
                'average_order_per_day' => $totalOrderPerDay,
            ],
            'tables' => [
                'top_product' => $topProduct,
                'top_category' => $topCategory,
                'newest_user' => $newestUser,
                'newest_order' => $newestOrder,
                'unpaid_order' => $unpaidOrder,
                'top_city' => $topCity,
                'top_user' => $topUser
            ],
            'chart' => [
                'age' => [
                    '20-25' => $firstAge,
                    '26-35' => $secondAge,
                    '36-50' => $thirdAge,
                    '>50' => $fourthAge
                ]
            ],
        ]);
    }
}
