<?php

namespace App\Http\Controllers\API\Promo\Flashsale;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Promo\Flashsale;
use App\Http\Requests\promo\CreateFlashsaleRequest;
use App\Http\Requests\promo\CreateFlashsaleItemsRequest;
use App\Http\Resources\Promo\IndexFlashsaleResources;
use App\Http\Resources\Promo\IndexMobileFlashsaleResources;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class FlashsaleController extends Controller
{
    public function store(CreateFlashsaleRequest $request)
    {
        $flashsale = Flashsale::create([
            'name' => $request->name,
            'image' => $request->file('image')->store('flashsale', 'public'),
            'start' => $request->start,
            'end' => $request->end,
        ]);
        return response()->json([
            'message' => 'Flashsale berhasil dibuat',
            'data' => $flashsale
        ], 201);
    }

    public function index()
    {
        $flashsale = QueryBuilder::for(Flashsale::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('name'),
                AllowedFilter::scope('is_active')
            ])
            ->allowedSorts([
                'id',
                'name'
            ])
            ->allowedIncludes([
                'items',
            ])
            ->defaultSort('-id')
            ->paginate(Request('limit') ?? 20);
        return response()->json([
            'message' => 'Flashsale berhasil ditampilkan',
            'data' => IndexFlashsaleResources::collection($flashsale),
            'pagination' => [
                'total' => $flashsale->total(),
                'per_page' => $flashsale->perPage(),
                'current_page' => $flashsale->currentPage(),
                'last_page' => $flashsale->lastPage(),
                'from' => $flashsale->firstItem(),
                'to' => $flashsale->lastItem()
            ]
        ], 200);
    }

    public function indexMobile()
    {
        $flashsale = QueryBuilder::for(Flashsale::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('name'),
                AllowedFilter::scope('is_active')
            ])
            ->allowedSorts([
                'id',
                'name'
            ])
            ->allowedIncludes([
                'items',
            ])
            ->defaultSort('-id')
            ->paginate(Request('limit') ?? 20);
        return response()->json([
            'message' => 'Flashsale berhasil ditampilkan',
            'data' => IndexMobileFlashsaleResources::collection($flashsale),
            'pagination' => [
                'total' => $flashsale->total(),
                'per_page' => $flashsale->perPage(),
                'current_page' => $flashsale->currentPage(),
                'last_page' => $flashsale->lastPage(),
                'from' => $flashsale->firstItem(),
                'to' => $flashsale->lastItem()
            ]
        ], 200);
    }

    public function update(CreateFlashsaleRequest $request, Flashsale $flashsale)
    {
        $flashsale->update([
            'name' => $request->name,
            'image' => $request->image ? $request->file('image')->store('flashsale', 'public') : $flashsale->image,
            'start' => $request->start,
            'end' => $request->end,
        ]);
        return response()->json([
            'message' => 'Flashsale berhasil diupdate',
            'data' => $flashsale
        ], 200);
    }

    public function destroy(Flashsale $flashsale)
    {
        $flashsale->delete();
        return response()->json([
            'message' => 'Flashsale berhasil dihapus',
            'data' => $flashsale
        ], 200);
    }

    public function updateFlashSaleItem(CreateFlashsaleItemsRequest $request, Flashsale $flashsale)
    {
        $skuIds = collect($request->items)->pluck('sku_id')->toArray();
        $flashsale->items()->whereNotIn('sku_id', $skuIds)->delete();
        foreach($request->items as $sku) {
            $flashsale->items()->updateOrCreate(
                ['sku_id' => $sku['sku_id']],
                ['discount' => $sku['discount']]
            );
        }
        return response()->json([
            'message' => 'Item berhasil ditambahkan',
            'data' => $flashsale->items
        ], 200);
    }
}
