<?php

namespace App\Http\Controllers\API\Promo\Voucher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Promo\Voucher;
use App\Models\Promo\VoucherClaim;
use App\Http\Requests\promo\CreateVoucherRequest;
use App\Http\Resources\Promo\IndexVoucherResources;
use App\Http\Resources\Promo\IndexVoucherMobileResources;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class VoucherController extends Controller
{
    public function store(CreateVoucherRequest $request){
        $voucher = Voucher::create($request->validated());

        return response()->json([
            'message' => 'Voucher created successfully',
            'status' => 'success',
            'data' => $voucher,
        ], 201);
    }

    public function index(){
        $data = QueryBuilder::for(Voucher::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('name'),
                AllowedFilter::exact('type'),
                AllowedFilter::exact('type_of_use'),
            ])
            ->allowedSorts([
                'id',
                'name',
                'type',
                'type_of_use',
            ])
            ->allowedIncludes([
                'voucherClaim',
            ])
            ->defaultSort('-id')
            ->paginate(Request('limit') ?? 20);

        return response()->json([
            'message' => 'Voucher list',
            'status' => 'success',
            'pagination' => [
                'total' => $data->total(),
                'count' => $data->count(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastItem()
            ],
            'data' => IndexVoucherResources::collection($data),
        ], 200);
    }

    public function update(CreateVoucherRequest $request, Voucher $voucher){
        $voucher->update($request->validated());

        return response()->json([
            'message' => 'Voucher updated successfully',
            'status' => 'success',
            'data' => $voucher,
        ], 200);
    }

    public function destroy(Voucher $voucher){
        $voucher->delete();

        return response()->json([
            'message' => 'Voucher deleted successfully',
            'status' => 'success',
        ], 200);
    }

    public function list(){
        $claimedVoucher = VoucherClaim::where('user_id', auth()->user()->id)->get();
        $data = QueryBuilder::for(Voucher::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('name'),
                AllowedFilter::exact('type'),
                AllowedFilter::exact('type_of_use'),
            ])
            ->allowedSorts([
                'id',
                'name',
                'type',
                'type_of_use',
            ])
            ->defaultSort('-id')
            ->paginate(Request('limit') ?? 20);
        if($claimedVoucher){
            foreach($data as $key => $voucher){
                foreach($claimedVoucher as $claimed){
                    if($voucher->id == $claimed['voucher_id']){
                        if($claimed['total_claim'] >= $voucher->max_claim_per_user){
                            unset($data[$key]);
                        }
                    }
                }
            }
        }

        return response()->json([
            'message' => 'Voucher list',
            'status' => 'success',
            'pagination' => [
                'total' => $data->total(),
                'count' => $data->count(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastItem()
            ],
            'data' => IndexVoucherMobileResources::collection($data),
        ], 200);
    }

    public function claim(Voucher $voucher){
        $oldVoucher = VoucherClaim::where('voucher_id', $voucher->id)
            ->where('user_id', auth()->user()->id)
            ->first();
        if($oldVoucher){
            if($oldVoucher->total_claim >= $voucher->max_claim_per_user){
                return response()->json([
                    'message' => 'You have reached the maximum claim for this voucher',
                    'status' => 'error',
                ], 400);
            }
            $oldVoucher->update([
                'total_claim' => $oldVoucher->total_claim + 1,
            ]);
        }
        else{
            $voucherClaim = VoucherClaim::create([
                'voucher_id' => $voucher->id,
                'user_id' => auth()->user()->id,
                'total_claim' => 1,
            ]);
        }
        return response()->json([
            'message' => 'Voucher claimed successfully',
            'status' => 'success',
            'data' => $oldVoucher ?? $voucherClaim,
        ], 200);
    }

    public function claimed(){
        $data = QueryBuilder::for(VoucherClaim::class)
            ->with('voucher')
            ->allowedFilters([
                AllowedFilter::exact('voucher.type_of_use'),
            ])
            ->defaultSort('-id')
            ->where('user_id', auth()->user()->id)
            ->paginate(Request('limit') ?? 20);

        return response()->json([
            'message' => 'Voucher claimed list',
            'status' => 'success',
            'pagination' => [
                'total' => $data->total(),
                'count' => $data->count(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastItem()
            ],
            'data' => IndexVoucherMobileResources::collection($data->load('voucher')->pluck('voucher')),
        ], 200);
    }
}
