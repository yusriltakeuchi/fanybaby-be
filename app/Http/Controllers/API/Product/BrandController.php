<?php

namespace App\Http\Controllers\API\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Models\Product\Brands;
use App\Http\Requests\product\BrandStoreRequest;
use App\Http\Resources\Product\IndexBrandResource;

class BrandController extends Controller
{
    public function index(){
        $data = QueryBuilder::for(Brands::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('name'),
            ])
            ->allowedSorts([
                'id',
                'name',
            ])
            ->defaultSort('name')
            ->paginate(Request('limit') ?? 20);

        return response()->json([
            'data' => IndexBrandResource::collection($data),
            'status' => 'success',
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastItem(),
            ]
        ]);
    }

    public function store(BrandStoreRequest $request){
        $data = $request->validated();
        $data['icon'] = $request->file('icon')->store('brands','public');
        $brand = Brands::create($data);

        return response()->json([
            'message' => 'Brand created',
            'data' => $brand,
            'status' => 'success',
        ]);
    }

    public function update(Brands $brands, BrandStoreRequest $request){
        $data = $request->validated();
        if($request->hasFile('icon')){
            $data['icon'] = $request->file('icon')->store('brands','public');
        }
        $brands->update($data);

        return response()->json([
            'message' => 'Brand updated',
            'data' => $brands,
            'status' => 'success',
        ]);
    }

    public function destroy(Brands $brands){
        $brands->delete();

        return response()->json([
            'message' => 'Brand deleted',
            'status' => 'success',
        ]);
    }
}
