<?php

namespace App\Http\Controllers\API\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product\Product;
use App\Models\Product\Wishlist;
use App\Http\Requests\product\WishlistAddRequest;
use App\Http\Resources\Product\IndexProductResource;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param WishlistAddRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addToWishlist(WishlistAddRequest $request)
    {
        $product = Product::find($request->product_id);
        $wishlist = Wishlist::where('user_id', auth()->user()->id)->where('product_id', $product->id)->first();
        if($wishlist) {
            return response()->json([
                'status' => 'error',
                'message' => 'Produk sudah ada di wishlist'
            ], 400);
        } else {
            $wishlist = Wishlist::create([
                'user_id' => auth()->user()->id,
                'product_id' => $product->id,
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'Produk berhasil ditambahkan ke wishlist',
                'data' => $wishlist
            ], 200);
        }
    }

    /**
     * return wishlist of user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function listWishlist()
    {
        $wishlist = Wishlist::where('user_id', auth()->user()->id)->with(['product' => function($query){$query->whereNull('deleted_at');}, 'product.skus'])->get();
        $wishlist = $wishlist->filter(function ($item) {
            return !is_null($item->product);
        });
        return response()->json([
            'status' => 'success',
            'message' => 'Daftar wishlist',
            'data' => IndexProductResource::collection($wishlist->pluck('product'))
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Product $product){
        $wishlist = Wishlist::where('user_id', auth()->user()->id)->where('product_id', $product->id)->first()->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'Produk berhasil dihapus dari wishlist',
        ], 200);
    }
}
