<?php

namespace App\Http\Controllers\API\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\ProductServices\ProductServices;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Http\Requests\product\ProductStoreRequest;
use App\Http\Requests\product\ProductUpdateRequest;
use App\Models\Product\Product;
use App\Models\Product\Sku;
use App\Http\Resources\Product\IndexProductResource;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Spatie\QueryBuilder\Exceptions\InvalidFilterQuery
     * @throws \Spatie\QueryBuilder\Exceptions\InvalidSortQuery
     * @throws \Spatie\QueryBuilder\Exceptions\InvalidIncludeQuery
     */
    public function index()
    {
        $data = QueryBuilder::for(Product::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('name'),
                AllowedFilter::exact('category_id'),
                AllowedFilter::exact('rating'),
                AllowedFilter::exact('brand_id'),
            ])
            ->allowedSorts([
                'id',
                'name',
                'rating',
                'total_rating',
            ])
            ->allowedIncludes([
                'skus',
                'category',
                'reviews',
                'wishlist',
                'brand'
            ])
            ->defaultSort('-id')
            ->paginate(Request('limit') ?? 20);

        return response()->json([
            'message' => 'Product list',
            'data' => IndexProductResource::collection($data),
            'status' => 'success',
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastItem()
            ]
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param ProductStoreRequest $request
     * @param ProductServices $services
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(ProductStoreRequest $request, ProductServices $services)
    {
        $product = $services->store($request);
        return response()->json([
            'message' => 'Product created successfully',
            'product' => $product,
            'status' => 'success'
        ], 201);
    }

    /**
     * Display the specified resource.
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Product $product)
    {
        return response()->json([
            'message' => 'Product details',
            'product' => $product,
            'status' => 'success'
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     * @param ProductUpdateRequest $request
     * @param ProductServices $services
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(ProductUpdateRequest $request, ProductServices $services, Product $product)
    {
        $product = $services->updateProduct($request, $product);
        return response()->json([
            'message' => 'Product updated successfully',
            'product' => $product,
            'status' => 'success'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json([
            'message' => 'Product deleted successfully',
            'status' => 'success'
        ], 200);
    }

    /**
     * delete SKU from product
     * @param Sku $sku
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroySku(Sku $sku)
    {
        $sku->delete();
        return response()->json([
            'message' => 'Sku deleted successfully',
            'status' => 'success'
        ], 200);
    }
}
