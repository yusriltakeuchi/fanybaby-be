<?php

namespace App\Http\Controllers\API\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product\Product;
use App\Models\Product\Review;
use App\Http\Requests\product\ReviewAddRequest;
use App\Http\Resources\Product\IndexReviewResource;

class ReviewController extends Controller
{
    /**
     * add review
     * @param Product $product
     * @param ReviewAddRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function addToReview(Product $product, ReviewAddRequest $request)
    {
        $review = Review::where('user_id', auth()->user()->id)->where('product_id', $product->id)->first();
        if($review) {
            return response()->json([
                'status' => 'error',
                'message' => 'Anda sudah memberikan review untuk produk ini'
            ], 400);
        }
        $image = $request->image ? $request->image->store('review', 'public') : null;
        $review = Review::create([
            'user_id' => auth()->user()->id,
            'product_id' => $product->id,
            'rating' => $request->rating,
            'comment' => $request->comment,
            'image' => $image,
        ]);
        $this->handleReviewChange($product);
        return response()->json([
            'status' => 'success',
            'message' => 'Review berhasil ditambahkan',
            'data' => $review
        ], 200);
    }

    /**
     * return review of user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function listReview()
    {
        $review = Review::where('user_id', auth()->user()->id)->with('product')->get();
        return response()->json([
            'status' => 'success',
            'message' => 'Daftar review',
            'data' => IndexReviewResource::collection($review)
        ], 200);
    }

    /**
     * delete review
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Product $product)
    {
        $review = Review::where('user_id', auth()->user()->id)->where('product_id', $product->id)->first()->delete();
        $this->handleReviewChange($product);
        return response()->json([
            'status' => 'success',
            'message' => 'Review berhasil dihapus',
        ], 200);
    }

    /**
     * return review of product
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function detailedReview(Product $product)
    {
        $review = Review::where('product_id', $product->id)->with('user')->paginate(request('limit') ?? 10);
        return response()->json([
            'status' => 'success',
            'message' => 'Daftar review',
            'data' => IndexReviewResource::collection($review)
        ], 200);
    }

    /**
     * handle review change
     * @param Product $product
     * @return void
     */
    private function handleReviewChange(Product $product)
    {
        $reviews = Review::where('product_id', $product->id)->get();
        if(count($reviews) == 0) {
            $product->rating = 0;
            $product->total_rating = 0;
            $product->save();
            return;
        } else {
            $total_rating = 0;
            foreach($reviews as $review) {
                $total_rating += $review->rating;
            }
            $product->rating = $total_rating / count($reviews);
            $product->total_rating = count($reviews);
            $product->save();
        }
    }
}
