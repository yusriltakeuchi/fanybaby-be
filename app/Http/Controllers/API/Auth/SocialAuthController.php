<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use App\Http\Services\DealposServices\Dealpos;

class SocialAuthController extends Controller
{
    public function loginWithToken(Request $request, string $provider, Dealpos $services){
        $user = Socialite::driver($provider)->userFromToken($request->token);
        $userCheck = User::where('email', $user->getEmail())->first();
        if($userCheck){
            $user = $userCheck;
        }else{
            $user = User::create([
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'email_verified_at' => now(),
                'password' => bcrypt('123456'),
                'google_id' => $provider == 'google' ? $user->getId() : null,
                'apple_id' => $provider == 'apple' ? $user->getId() : null,
            ]);
        }
        $services->checkForRegisteredEmail($user);
        $token = auth()->login($user);
        return response()->json([
            'status' => 'success',
            'data' => [
                'token' => $token,
                'user' => $user
            ]
        ]);
    }
}
