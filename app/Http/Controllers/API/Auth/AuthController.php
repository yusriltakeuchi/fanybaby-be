<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\User;
use App\Http\Requests\auth\RegisterRequest;
use App\Http\Requests\auth\OTPRequest;
use App\Http\Requests\auth\ForgotRequest;
use App\Http\Services\WAServices\OTPWA;
use App\Http\Services\DealposServices\Dealpos;
use App\Models\Auth\OTP;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'resend', 'verify', 'forgot', 'verifyForgot', 'validatePhone', 'logout']]);
    }

    public function login(Dealpos $services)
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $services->checkForRegisteredEmail(auth()->user());
        return response()->json([
            'data' => [
                'token' => $token,
                'verification_status' => (auth()->user()->phone_verified_at != null) || (auth()->user()->email_verified_at != null) ? true : false,
            ],
            'message' => 'Login successful',
            'status' => 'success'
        ]);
    }

    public function me(Dealpos $services)
    {
        $user = auth()->user();
        $point = $services->getPoint($user);
        return response()->json([
            'data' => [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone,
                'dob' => $user->dob,
                'is_admin' => $user->is_admin,
                'avatar' => url('storage/'.$user->avatar),
                'point' => $point,
            ],
            'message' => 'User data',
            'status' => 'success'
        ]);
    }

    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function register(RegisterRequest $request, OTPWA $services)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone' => $request->phone,
        ]);
        $token = JWTAuth::fromUser($user);
        if($services->sendOTP($request->phone, $user->id) == false){
            return response()->json([
                'message' => 'Failed to send OTP',
                'status' => 'error'
            ], 401);
        }
        return response()->json([
            'data' => ['token' => $token],
            'message' => 'Register successful, but you need to verify your phone number, otp has been sent to your phone number',
            'status' => 'success'
        ]);
    }

    public function verify(OTPRequest $request, Dealpos $services){
        $user = auth()->user();
        if($user->phone_verified_at === null){
            $otp = OTP::where('phone', $user->phone)->where('code', $request->code)->where('expires_at', '>', now())->latest()->first();
            if(!$otp){
                return response()->json([
                    'message' => 'Permintaan OTP tidak ditemukan',
                    'status' => 'error'
                ], 401);
            }
            if($otp->code == $request->code){
                $user->phone_verified_at = now();
                $user->save();
                $otp->verified_at = now();
                $otp->save();
                $services->checkForRegisteredEmail($user);
                return response()->json([
                    'message' => 'User verified',
                    'status' => 'success'
                ]);
            }else{
                return response()->json([
                    'message' => 'OTP not match',
                    'status' => 'error'
                ], 401);
            }
        }else{
            return response()->json([
                'message' => 'User already verified',
                'status' => 'error'
            ], 401);
        }
    }

    public function forgot(OTPWA $services, Request $request){
        $user = User::where('phone', $request->phone)->first();
        if(!$user){
            return response()->json([
                'message' => 'User not found',
                'status' => 'error'
            ], 401);
        }
        return response()->json([
            'message' => 'OTP has been sent to your phone number',
            'data' => ['otp_sent' => $services->sendForgotOTP($request->phone)],
            'status' => 'success'
        ]);
    }

    public function verifyForgot(ForgotRequest $request){
        $otp = OTP::where('phone', $request->phone)->where('code', $request->code)->where('expires_at', '>', now())->latest()->first();
        if($otp->code == $request->code){
            $user = User::where("phone",$request->phone)->first();
            $user->password = bcrypt($request->new_password);
            $user->save();
            $otp->verified_at = now();
            $otp->save();
            return response()->json([
                'message' => 'Password Updated',
                'status' => 'success'
            ]);
        }else{
            return response()->json([
                'message' => 'OTP not match',
                'status' => 'error'
            ], 401);
        }
    }

    public function resend(OTPWA $services, Request $request){
        $phone = $request->phone ? $request->phone : auth()->user()->phone;
        $id = $request->phone ? User::where('phone', $request->phone)->first()->id : auth()->user()->id;
        $services->sendOTP($phone, $id, true);
        return response()->json([
            'message' => 'New OTP has been sent to your phone number',
            'status' => 'success'
        ]);
    }

    public function validatePhone(OTPRequest $request){
        $otp = OTP::where('phone', $request->phone)->where('code', $request->code)->where('expires_at', '>', now())->latest()->first();
        if($otp){
            return response()->json([
                'message' => 'OTP valid',
                'status' => 'success'
            ]);
        }else{
            return response()->json([
                'message' => 'OTP not valid',
                'status' => 'error'
            ], 401);
        }
    }

    public function saveToken(){
        $user = auth()->user();
        $user->fcm_token = request('fcm_token');
        $user->save();
        return response()->json([
            'message' => 'Token saved',
            'status' => 'success'
        ]);
    }
}
