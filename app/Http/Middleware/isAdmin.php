<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->user() === null) {
            return response()->json([
                'message' => 'Unauthenticated',
                'status' => 'error'
            ], 401);
        }
        if ($request->user()->is_admin !== 1) {
            return response()->json([
                'message' => 'You are not authorized to access this resource',
            ], 403);
        }

        return $next($request);
    }
}
