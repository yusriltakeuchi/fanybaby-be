<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $icon = str_contains($this->icon, 'https') == true ? $this->icon : url('storage/'.$this->icon);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'icon' => $icon,
            'is_featured' => $this->is_featured,
            'is_parent' => $this->is_parent,
            'parent_id' => $this->parent_id ?? null,
            'children' => $this->is_parent == 1 ? IndexCategoryResource::collection($this->children) : null,
        ];
    }
}
