<?php

namespace App\Http\Resources\Banner;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexBannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        if (str_contains($this->image, 'https') == true) {
            return [
                'id' => $this->id,
                'name' => $this->name,
                'image' => $this->image,
                'link' => $this->link,
                'type' => $this->type,
            ];
        } else {
            return [
                'id' => $this->id,
                'name' => $this->name,
                'image' => url('storage/' . $this->image),
                'link' => $this->link,
                'type' => $this->type,
            ];
        }
    }
}
