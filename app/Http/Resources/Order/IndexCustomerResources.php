<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexCustomerResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'customer_name' => $this->customer_name,
            'subdistrict' => $this->subdistrict->name,
            'subdistrict_id' => $this->subdistrict_id,
            'district' => $this->subdistrict->district->name,
            'city' => $this->subdistrict->city->name,
            'province' => $this->subdistrict->province->name,
            'address' => $this->address,
            'postal_code' => $this->subdistrict->postal_code,
            'phone' => $this->phone,
            'is_primary' => $this->is_primary == 1 ? true : false,
            'lat' => $this->lat ?? null,
            'long' => $this->long ?? null,
            'icon' => $this->icon ?? null,
        ];
    }
}
