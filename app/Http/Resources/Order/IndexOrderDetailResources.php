<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexOrderDetailResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        foreach ($this->orderItems as $orderItems) {
            $image = str_contains($orderItems->sku->product->image, 'https') == true ? $orderItems->sku->product->image : url('storage/'.$orderItems->sku->product->image);
            $orderProduct[] = [
                'id' => $orderItems->sku->product->id,
                'name' => $orderItems->sku->product->name,
                'image' => $image,
                'properties' => $orderItems->sku->properties,
                'quantity' => $orderItems->quantity,
                'price' => $orderItems->sku->price,
            ];
        }
        $voucher = 0;
        if($this->voucher && $this->total != null) {
            $voucher = $this->subtotal - $this->total;
        }
        return [
            'id' => $this->id,
            'date' => $this->created_at->format('d M Y, H:i'),
            'product' => $orderProduct,
            'shipping_info' => [
                'courier' => $this->courier->name,
                'awb' => $this->awb ?? '-',
                'address' => $this->customer->title.', '.$this->customer->address.', '.$this->customer->subdistrict->name.', '.$this->customer->subdistrict->city->name.', '.$this->customer->subdistrict->city->province->name.' '.$this->customer->subdistrict->postal_code,
            ],
            'payment_info' => [
                'method' => $this->orderPayment->payment_method,
                'subtotal' => $this->subtotal,
                'shipping_cost' => $this->shipping_fee,
                'voucher' => $voucher,
                'total' => $this->total + $this->shipping_fee,
                'snap_token' => $this->orderPayment->midtrans_snap_token ?? '',
            ],
            'order_status' => $this->status,
        ];
    }
}
