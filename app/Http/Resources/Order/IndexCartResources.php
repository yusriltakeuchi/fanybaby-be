<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Product\IndexSkuResource;
use App\Http\Resources\Product\IndexProductResource;

class IndexCartResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'quantity' => $this->quantity,
            'product' => [
                'data' => new IndexProductResource($this->sku->product),
                'sku' => new IndexSkuResource($this->sku),
            ],
        ];
    }
}
