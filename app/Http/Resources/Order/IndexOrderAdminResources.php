<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexOrderAdminResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        foreach ($this->orderHistory as $orderHistory) {
            $orderStatus[] = [
                'id' => $orderHistory->id,
                'status' => $orderHistory->status
            ];
        }
        foreach ($this->orderItems as $orderItems) {
            $image = str_contains($orderItems->sku->product->image, 'https') == true ? $orderItems->sku->product->image : url('storage/'.$orderItems->sku->product->image);
            $orderProduct[] = [
                'id' => $orderItems->sku->product->id,
                'name' => $orderItems->sku->product->name,
                'image' => $image,
                'properties' => $orderItems->sku->properties,
                'quantity' => $orderItems->quantity,
                'price' => $orderItems->sku->price,
                'weight_gram' => $orderItems->sku->weight_gram,
            ];
        }
        $voucher = 0;
        if($this->voucher && $this->total != null) {
            $voucher = $this->subtotal - $this->total;
        }
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'customer' => [
                'id' => $this->customer->id,
                'name' => $this->customer->customer_name,
                'address' => $this->customer->address.', '.$this->customer->subdistrict->name.', '.$this->customer->subdistrict->city->name.', '.$this->customer->subdistrict->city->province->name.' '.$this->customer->subdistrict->postal_code,
            ],
            'items' => $orderProduct,
            'history' => $orderStatus,
            'shipping_info' => [
                'courier' => $this->courier->name,
                'awb' => $this->awb ?? '-',
                'icon' => $this->courier->icon_url
            ],
            'payment_info' => [
                'method' => $this->orderPayment->payment_method,
                'subtotal' => $this->subtotal,
                'shipping_cost' => $this->shipping_fee,
                'voucher' => $voucher,
                'voucher_name' => $this->voucher->name ?? '',
                'total' => $this->total + $this->shipping_fee,
            ],
            'is_cod' => $this->is_cod,
            'is_downloaded' => $this->is_downloaded,
        ];
    }
}