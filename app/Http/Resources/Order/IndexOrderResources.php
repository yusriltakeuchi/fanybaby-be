<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexOrderResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        foreach ($this->orderItems as $orderItems) {
            $image = str_contains($orderItems->sku->product->image, 'https') == true ? $orderItems->sku->product->image : url('storage/'.$orderItems->sku->product->image);
            $orderProduct[] = [
                'id' => $orderItems->sku->product->id,
                'name' => $orderItems->sku->product->name,
                'image' => $image,
                'properties' => $orderItems->sku->properties,
                'quantity' => $orderItems->quantity,
                'price' => $orderItems->sku->price,
            ];
        }

        return [
            'id' => $this->id,
            'order_status' => $this->status,
            'product' => $orderProduct,
            'subtotal' => $this->subtotal,
            'awb' => $this->awb ?? '-',
            'created_at' => $this->created_at,
        ];
    }
}
