<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $this->user->name = substr($this->user->name, 0, 1) . str_repeat('*', strlen($this->user->name) - 2) . substr($this->user->name, -1);
        return [
            'id' => $this->id,
            'user' => $this->user->name,
            'product' => $this->product_id,
            'rating' => $this->rating,
            'comment' => $this->comment,
            'image' => url('storage/'.$this->image) ?? null,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}
