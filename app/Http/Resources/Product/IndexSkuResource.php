<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexSkuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $image = str_contains($this->image, 'https') == true ? $this->image : url('storage/'.$this->image);
        return [
            'id' => $this->id,
            'code' => $this->code,
            'image' => $image,
            'price' => $this->price,
            'properties' => $this->properties,
            'stock' => $this->stock,
            'weight_gram' => $this->weight_gram,
        ];
    }
}
