<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Category\IndexCategoryResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $image = true == str_contains($this->image, 'https') ? $this->image : url('storage/'.$this->image);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'total_rating' => $this->total_rating,
            'rating' => $this->rating,
            'description' => $this->description,
            'image' => $image,
            'total_sell' => $this->total_sell,
            'wishlist' => $this->whenLoaded('wishlist', function () {
                if ($this->wishlist->count() > 0) {
                    return true;
                }

                return false;
            }),
            'brand' => $this->whenLoaded('brand', function () {
                return new IndexBrandResource($this->brand);
            }),
            'category' => $this->whenLoaded('category', function () {
                return new IndexCategoryResource($this->category);
            }),
            'skus' => $this->whenLoaded('skus', function () {
                return IndexSkuResource::collection($this->skus);
            }),
            'total_review' => $this->whenLoaded('reviews', function () {
                return $this->reviews->count();
            }),
            'reviews' => $this->whenLoaded('reviews', function () {
                return IndexReviewResource::collection($this->reviews->take(4));
            }),
        ];
    }
}
