<?php

namespace App\Http\Resources\Promo;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexFlashsaleResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => url('storage/'.$this->image),
            'start' => $this->start,
            'end' => $this->end,
            'items' => $this->whenLoaded('items', function () {
                return $this->items->map(function ($item) {
                        if($item->sku != null){
                            return [
                                'id' => $item->id,
                                'sku_id' => $item->sku_id,
                                'discount' => $item->discount,
                                'sku' => [
                                    'id' => $item->sku->id,
                                    'properties' => $item->sku->properties,
                                    'price' => $item->sku->price,
                                    'image' => url('storage/'.$item->sku->image),
                                    'stock' => $item->sku->stock,
                                ],
                                'product' => [
                                    'id' => $item->sku->product->id,
                                    'name' => $item->sku->product->name,
                                    'slug' => $item->sku->product->slug,
                                    'image' => url('storage/'.$item->sku->product->image),
                                ],
                            ];
                        }else{
                            return [
                                'id' => $item->id,
                                'sku_id' => $item->sku_id,
                                'discount' => $item->discount,
                                'sku' => [],
                                'product' => [
                                    'id' => $item->sku_id,
                                    'name' => 'Tidak ada',
                                    'slug' => 'Tidak ada',
                                    'image' => 'Tidak ada',
                                ],
                            ];
                        }
                });
            }),
        ];
    }
}
