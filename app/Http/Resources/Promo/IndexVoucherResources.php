<?php

namespace App\Http\Resources\Promo;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexVoucherResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'type' => $this->type,
            'max_claim_total' => $this->max_claim_total,
            'max_claim_per_user' => $this->max_claim_per_user,
            'type_of_use' => $this->type_of_use,
            'min_total' => $this->min_total,
            'max_discount' => $this->max_discount,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'created_at' => $this->created_at->diffForHumans(),
            'voucher_claim' => $this->whenLoaded('voucherClaim', function () {
                return $this->voucherClaim->map(function ($item) {
                    return [
                        'id' => $item->user->id,
                        'name' => $item->user->name,
                        'image' => url('storage/' . $item->user->avatar),
                    ];
                });
            }),
        ];
    }
}
