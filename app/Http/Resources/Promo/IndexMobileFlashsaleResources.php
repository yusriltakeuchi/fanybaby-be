<?php

namespace App\Http\Resources\Promo;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexMobileFlashsaleResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => url('storage/'.$this->image),
            'start' => $this->start,
            'end' => $this->end,
            'items' => $this->whenLoaded('items', function () {
                return $this->items->map(function ($item) {
                    return [
                        'id' => $item->sku->product->id,
                        'name' => $item->sku->product->name,
                        'image' => url('storage/'.$item->sku->product->image),
                        'total_rating' => $item->sku->product->total_rating,
                        'rating' => $item->sku->product->rating,
                        'total_sell' => $item->sku->product->total_sell,
                        'sku' => [
                            'id' => $item->sku->id,
                            'properties' => $item->sku->properties,
                            'price_after_disc' => $item->sku->price - $item->discount,
                            'price_before_disc' => intval($item->sku->price),
                            'image' => url('storage/'.$item->sku->image),
                            'price' => $item->sku->price,
                            'stock' => $item->sku->stock,
                            'weight_gram' => $item->sku->weight_gram,
                        ]
                    ];
                })->unique('id');
            }),
        ];
    }
}
