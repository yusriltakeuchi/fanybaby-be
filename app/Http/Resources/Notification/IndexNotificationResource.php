<?php

namespace App\Http\Resources\Notification;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexNotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title ?? '',
            'body' => $this->body ?? '',
            'is_whatsapp' => $this->is_whatsapp ?? '',
            'is_push_notification' => $this->is_push_notification ?? '',
            'notifiable_id' => $this->notifiable_id ?? '',
            'notifiable_type' => $this->notifiable_type ?? '',
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}
