<?php

namespace App\Http\Requests\category;

use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'icon' => 'required|file|max:1024',
            'is_featured' => 'required|boolean',
            'is_parent' => 'required|boolean',
            'parent_id' => 'nullable|integer',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Nama Kategori harus diisi',
            'name.string' => 'Nama Kategori harus berupa string',
            'name.max' => 'Nama Kategori maksimal 255 karakter',
            'icon.required' => 'Icon Kategori harus diisi',
            'icon.string' => 'Icon Kategori harus berupa string',
            'icon.max' => 'Icon Kategori maksimal 255 karakter',
            'is_featured.required' => 'Status Featured Kategori harus diisi',
            'is_featured.boolean' => 'Status Featured Kategori harus berupa boolean',
            'is_parent.required' => 'Status Parent Kategori harus diisi',
            'is_parent.boolean' => 'Status Parent Kategori harus berupa boolean',
            'parent_id.integer' => 'Parent ID Kategori harus berupa integer',
        ];
    }
}
