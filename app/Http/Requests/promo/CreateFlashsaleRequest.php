<?php

namespace App\Http\Requests\promo;

use Illuminate\Foundation\Http\FormRequest;

class CreateFlashsaleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'image' => 'nullable|image',
            'start' => 'required|date_format:Y-m-d\TH:i',
            'end' => 'required|date_format:Y-m-d\TH:i',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Nama flashsale harus diisi',
            'image.image' => 'Gambar flashsale harus berupa gambar',
            'start.required' => 'Waktu mulai flashsale harus diisi',
            'start.date_format' => 'Waktu mulai flashsale harus berformat Y-m-d\TH:i',
            'end.required' => 'Waktu berakhir flashsale harus diisi',
            'end.date_format' => 'Waktu berakhir flashsale harus berformat Y-m-d\TH:i',
        ];
    }
}
