<?php

namespace App\Http\Requests\promo;

use Illuminate\Foundation\Http\FormRequest;

class CreateFlashsaleItemsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'items' => 'required|array',
            'items.*.sku_id' => 'required|exists:skus,id',
            'items.*.discount' => 'required|integer',
        ];
    }

    public function messages(){
        return [
            'items.required' => 'Item flashsale harus diisi',
            'items.array' => 'Item flashsale harus berupa array',
            'items.*.sku_id.required' => 'SKU harus diisi',
            'items.*.sku_id.exists' => 'SKU tidak ditemukan',
            'items.*.discount.required' => 'Diskon harus diisi',
            'items.*.discount.integer' => 'Diskon harus berupa angka',
        ];
    }
}
