<?php

namespace App\Http\Requests\promo;

use Illuminate\Foundation\Http\FormRequest;

class CreateVoucherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'value' => 'required|numeric',
            'type' => 'required|string|in:percent,nominal',
            'max_claim_total' => 'required|numeric',
            'max_claim_per_user' => 'required|numeric',
            'type_of_use' => 'required|string|in:discount,shipping',
            'min_total' => 'required|numeric',
            'max_discount' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'name.string' => 'Nama harus berupa string',
            'value.required' => 'Nilai tidak boleh kosong',
            'value.numeric' => 'Nilai harus berupa angka',
            'type.required' => 'Tipe tidak boleh kosong',
            'type.string' => 'Tipe harus berupa string',
            'type.in' => 'Tipe harus berupa percent atau nominal',
            'max_claim_total.required' => 'Maksimal klaim total tidak boleh kosong',
            'max_claim_total.numeric' => 'Maksimal klaim total harus berupa angka',
            'max_claim_per_user.required' => 'Maksimal klaim per user tidak boleh kosong',
            'max_claim_per_user.numeric' => 'Maksimal klaim per user harus berupa angka',
            'type_of_use.required' => 'Tipe penggunaan tidak boleh kosong',
            'type_of_use.string' => 'Tipe penggunaan harus berupa string',
            'type_of_use.in' => 'Tipe penggunaan harus berupa discount atau shipping',
            'min_total.required' => 'Minimal total tidak boleh kosong',
            'min_total.numeric' => 'Minimal total harus berupa angka',
            'max_discount.required' => 'Maksimal diskon tidak boleh kosong',
            'max_discount.numeric' => 'Maksimal diskon harus berupa angka',
            'start_date.required' => 'Tanggal mulai tidak boleh kosong',
            'start_date.date' => 'Tanggal mulai harus berupa tanggal',
            'end_date.required' => 'Tanggal berakhir tidak boleh kosong',
            'end_date.date' => 'Tanggal berakhir harus berupa tanggal',
        ];
    }
}
