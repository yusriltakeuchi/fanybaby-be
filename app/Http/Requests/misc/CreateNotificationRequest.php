<?php

namespace App\Http\Requests\misc;

use Illuminate\Foundation\Http\FormRequest;

class CreateNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'body' => 'required|string',
            'is_whatsapp' => 'required|boolean',
            'is_push_notification' => 'required|boolean',
            'notifiable_id' => 'nullable|integer',
            'notifiable_type' => 'nullable|string',
        ];
    }

    public function messages(){
        return [
            'title.required' => 'Judul tidak boleh kosong',
            'body.required' => 'Isi tidak boleh kosong',
            'is_whatsapp.required' => 'Status Whatsapp tidak boleh kosong',
            'is_push_notification.required' => 'Status Push Notification tidak boleh kosong',
            'is_whatsapp.boolean' => 'Status Whatsapp harus berupa boolean',
            'is_push_notification.boolean' => 'Status Push Notification harus berupa boolean',
            'notifiable_id.integer' => 'Notifiable ID harus berupa integer',
            'notifiable_type.string' => 'Notifiable Type harus berupa string',
        ];
    }
}
