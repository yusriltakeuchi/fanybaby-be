<?php

namespace App\Http\Requests\banner;

use Illuminate\Foundation\Http\FormRequest;

class BannerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'image' => 'nullable|file|max:1024',
            'link' => 'nullable|string|max:255',
            'type' => 'nullable|string|max:40|in:top,bottom,recomendation,categories,flashsale,login,register',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Nama Banner harus diisi',
            'name.string' => 'Nama Banner harus berupa string',
            'name.max' => 'Nama Banner maksimal 255 karakter',
            'image.required' => 'Gambar Banner harus diisi',
            'image.string' => 'Gambar Banner harus berupa string',
            'image.max' => 'Gambar Banner maksimal 255 karakter',
            'link.string' => 'Link Banner harus berupa string',
            'link.max' => 'Link Banner maksimal 255 karakter',
            'type.string' => 'Tipe Banner harus berupa string',
        ];
    }
}
