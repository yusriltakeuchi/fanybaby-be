<?php

namespace App\Http\Requests\order;

use Illuminate\Foundation\Http\FormRequest;

class CartStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'sku_id' => 'required|exists:skus,id',
            'quantity' => 'required|numeric|min:1'
        ];
    }

    public function messages(){
        return [
            'sku_id.required' => 'SKU ID harus diisi',
            'sku_id.exists' => 'SKU ID tidak ditemukan',
            'quantity.required' => 'Quantity harus diisi',
            'quantity.numeric' => 'Quantity harus berupa angka',
            'quantity.min' => 'Quantity minimal 1',
        ];
    }
}
