<?php

namespace App\Http\Requests\order;

use Illuminate\Foundation\Http\FormRequest;

class PlaceOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'sku' => 'required|array',
            'sku.*.id' => 'required|integer|exists:skus,id',
            'sku.*.quantity' => 'required|integer|min:1',
            'voucher_id' => 'nullable|integer|exists:voucher,id',
            'courier_code' => 'required|string|exists:couriers,code',
            'courier_service' => 'required|string',
            'courier_cost' => 'required|integer',
            'customer_id' => 'required|integer|exists:customer,id',
            'is_cod' => 'nullable|boolean',
        ];
    }

    public function messages(): array
    {
        return [
            'sku.required' => 'SKU tidak boleh kosong',
            'sku.*.id.required' => 'SKU tidak boleh kosong',
            'sku.*.id.integer' => 'SKU harus berupa angka',
            'sku.*.id.exists' => 'SKU tidak ditemukan',
            'sku.*.quantity.required' => 'Jumlah tidak boleh kosong',
            'sku.*.quantity.integer' => 'Jumlah harus berupa angka',
            'sku.*.quantity.min' => 'Jumlah minimal 1',
            'voucher_id.integer' => 'Voucher harus berupa angka',
            'voucher_id.exists' => 'Voucher tidak ditemukan',
            'courier_code.required' => 'Kode kurir tidak boleh kosong',
            'courier_code.integer' => 'Kode kurir harus berupa angka',
            'courier_code.exists' => 'Kode kurir tidak ditemukan',
            'courier_service.required' => 'Layanan kurir tidak boleh kosong',
            'courier_service.string' => 'Layanan kurir harus berupa string',
            'courier_cost.required' => 'Biaya kurir tidak boleh kosong',
            'courier_cost.integer' => 'Biaya kurir harus berupa angka',
            'customer_id.required' => 'ID customer tidak boleh kosong',
            'customer_id.integer' => 'ID customer harus berupa angka',
            'customer_id.exists' => 'ID customer tidak ditemukan',
            'is_cod.boolean' => 'COD harus berupa boolean',
        ];
    }
}
