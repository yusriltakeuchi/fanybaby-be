<?php

namespace App\Http\Requests\order;

use Illuminate\Foundation\Http\FormRequest;

class CustomerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'customer_name' => 'required|string',
            'subdistrict_id' => 'required|integer|exists:area_subdistricts,id',
            'address' => 'required|string',
            'phone' => 'required|string',
            'is_primary' => 'required|boolean',
            'lat' => 'nullable|string',
            'long' => 'nullable|string',
            'icon' => 'nullable|string',
        ];
    }

    public function messages(){
        return [
            'title.required' => 'Title tidak boleh kosong',
            'title.string' => 'Title harus berupa string',
            'customer_name.required' => 'Nama tidak boleh kosong',
            'customer_name.string' => 'Nama harus berupa string',
            'subdistrict_id.required' => 'Kota tidak boleh kosong',
            'subdistrict_id.integer' => 'Kota harus berupa angka',
            'subdistrict_id.exists' => 'Kota tidak ditemukan',
            'address.required' => 'Alamat tidak boleh kosong',
            'address.string' => 'Alamat harus berupa string',
            'phone.required' => 'Nomor telepon tidak boleh kosong',
            'phone.string' => 'Nomor telepon harus berupa string',
            'is_primary.boolean' => 'Is primary harus berupa boolean',
            'is_primary.required' => 'Is primary tidak boleh kosong',
            'lat.string' => 'Latitude harus berupa string',
            'long.string' => 'Longitude harus berupa string',
            'icon.string' => 'Icon harus berupa string',
        ];
    }
}
