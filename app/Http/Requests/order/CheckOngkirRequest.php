<?php

namespace App\Http\Requests\order;

use Illuminate\Foundation\Http\FormRequest;

class CheckOngkirRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'weight' => 'required|integer',
            'origin' => 'required|integer|exists:area_subdistricts,id',
            'destination' => 'required|integer|exists:area_subdistricts,id',
        ];
    }

    public function messages(){
        return [
            'weight.required' => 'Berat tidak boleh kosong',
            'weight.integer' => 'Berat harus berupa angka',
            'origin.required' => 'Kota asal tidak boleh kosong',
            'origin.integer' => 'Kota asal harus berupa angka',
            'origin.exists' => 'Kota asal tidak ditemukan',
            'destination.required' => 'Kota tujuan tidak boleh kosong',
            'destination.integer' => 'Kota tujuan harus berupa angka',
            'destination.exists' => 'Kota tujuan tidak ditemukan',
        ];
    }
}
