<?php

namespace App\Http\Requests\product;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'description' => 'required|string',
            'category_id' => 'required|integer|exists:category,id',
            'image' => 'nullable|image',
            'skus' => 'required|array',
            'skus.*.code' => 'required|string',
            'skus.*.price' => 'required|integer',
            'skus.*.stock' => 'required|integer',
            'skus.*.image' => 'nullable|image',
            'skus.*.weight_gram' => 'required|integer',
            'skus.*.properties' => 'required|array',
            'brand_id' => 'nullable|integer|exists:brands,id',
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Nama produk harus diisi',
            'name.string' => 'Nama produk harus berupa string',
            'description.required' => 'Deskripsi produk harus diisi',
            'description.string' => 'Deskripsi produk harus berupa string',
            'category_id.required' => 'Kategori produk harus diisi',
            'category_id.integer' => 'Kategori produk harus berupa integer',
            'image.image' => 'Gambar produk harus berupa gambar',
            'skus.required' => 'SKU produk harus diisi',
            'skus.array' => 'SKU produk harus berupa array',
            'skus.*.code.required' => 'Kode SKU produk harus diisi',
            'skus.*.code.string' => 'Kode SKU produk harus berupa string',
            'skus.*.price.required' => 'Harga SKU produk harus diisi',
            'skus.*.price.integer' => 'Harga SKU produk harus berupa integer',
            'skus.*.stock.required' => 'Stok SKU produk harus diisi',
            'skus.*.stock.integer' => 'Stok SKU produk harus berupa integer',
            'skus.*.image.image' => 'Gambar SKU produk harus berupa gambar',
            'skus.*.weight_gram.required' => 'Berat SKU produk harus diisi',
            'skus.*.properties.required' => 'Properti SKU produk harus diisi',
            'skus.*.properties.array' => 'Properti SKU produk harus berupa array',
            'brand_id.integer' => 'Brand produk harus berupa integer',
            'brand_id.exists' => 'Brand produk tidak ditemukan',
        ];
    }
}
