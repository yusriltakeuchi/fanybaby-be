<?php

namespace App\Http\Requests\product;

use Illuminate\Foundation\Http\FormRequest;

class BrandStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'icon' => 'required|file|mimes:png,jpg,jpeg|max:2048',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'icon.required' => 'Icon tidak boleh kosong',
            'icon.file' => 'Icon harus berupa file',
            'icon.mimes' => 'Icon harus berupa png, jpg, jpeg',
            'icon.max' => 'Icon maksimal 2MB',
        ];
    }
}
