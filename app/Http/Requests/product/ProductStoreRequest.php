<?php

namespace App\Http\Requests\product;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'description' => 'required|string',
            'category_id' => 'required|integer|exists:category,id',
            'image' => 'required|image',
            'skus' => 'required|array',
            'skus.*.code' => 'required|string',
            'skus.*.price' => 'required|integer',
            'skus.*.stock' => 'required|integer',
            'skus.*.image' => 'required|image',
            'skus.*.properties' => 'required|array',
            'skus.*.weight_gram' => 'required|integer',
            'brand_id' => 'nullable|integer|exists:brands,id',
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Nama produk harus diisi',
            'name.string' => 'Nama produk harus berupa string',
            'description.required' => 'Deskripsi produk harus diisi',
            'description.string' => 'Deskripsi produk harus berupa string',
            'category_id.required' => 'Kategori produk harus diisi',
            'category_id.integer' => 'Kategori produk harus berupa integer',
            'image.required' => 'Gambar produk harus diisi',
            'image.image' => 'Gambar produk harus berupa gambar',
            'skus.required' => 'SKU produk harus diisi',
            'skus.array' => 'SKU produk harus berupa array',
            'skus.*.code.required' => 'Kode SKU produk harus diisi',
            'skus.*.code.string' => 'Kode SKU produk harus berupa string',
            'skus.*.price.required' => 'Harga SKU produk harus diisi',
            'skus.*.price.integer' => 'Harga SKU produk harus berupa integer',
            'skus.*.stock.required' => 'Stok SKU produk harus diisi',
            'skus.*.stock.integer' => 'Stok SKU produk harus berupa integer',
            'skus.*.image.required' => 'Gambar SKU produk harus diisi',
            'skus.*.image.image' => 'Gambar SKU produk harus berupa gambar',
            'skus.*.properties.required' => 'Properti SKU produk harus diisi',
            'skus.*.properties.array' => 'Properti SKU produk harus berupa array',
            'skus.*.weight_gram.integer' => 'Berat SKU produk harus berupa integer',
            'brand_id.integer' => 'Brand produk harus berupa integer',
            'brand_id.exists' => 'Brand produk tidak ditemukan',
        ];
    }
}
