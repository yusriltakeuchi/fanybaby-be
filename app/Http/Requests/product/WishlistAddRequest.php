<?php

namespace App\Http\Requests\product;

use Illuminate\Foundation\Http\FormRequest;

class WishlistAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'product_id' => 'required|exists:products,id',
        ];
    }

    public function messages(): array
    {
        return [
            'product_id.required' => 'Produk tidak boleh kosong',
            'product_id.exists' => 'Produk tidak ditemukan',
        ];
    }
}
