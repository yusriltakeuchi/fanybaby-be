<?php

namespace App\Http\Requests\auth;

use Illuminate\Foundation\Http\FormRequest;

class ForgotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'code' => 'required|string:6',
            'new_password' => 'required|string|min:6',
            'phone' => 'required|string'
        ];
    }

    public function messages(){
        return [
            'code.required' => 'Kode OTP tidak boleh kosong',
            'code.string' => 'Kode OTP harus berupa string',
            'code.size' => 'Kode OTP harus berjumlah 6 karakter',
            'new_password.required' => 'Password tidak boleh kosong',
            'new_password.string' => 'Password harus berupa string',
            'new_password.min' => 'Password minimal 6 karakter',
            'phone.required' => 'Nomor Telp Diperlukan',
            'phone.string' => 'phone harus berupa string'
        ];
    }
}
