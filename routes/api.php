<?php

use App\Http\Controllers\API\Analytic\AnalyticAdminController;
use App\Http\Controllers\API\Auth\AuthController;
use App\Http\Controllers\API\Auth\SocialAuthController;
use App\Http\Controllers\API\Banner\BannerController;
use App\Http\Controllers\API\Category\CategoryController;
use App\Http\Controllers\API\Misc\ExportController;
use App\Http\Controllers\API\Misc\NotificationController;
use App\Http\Controllers\API\MiscController;
use App\Http\Controllers\API\Order\CartController;
use App\Http\Controllers\API\Order\CustomerController;
use App\Http\Controllers\API\Order\OngkirController;
use App\Http\Controllers\API\Order\OrderAdminController;
use App\Http\Controllers\API\Order\OrderController;
use App\Http\Controllers\API\Product\BrandController;
use App\Http\Controllers\API\Product\ProductController;
use App\Http\Controllers\API\Product\ReviewController;
use App\Http\Controllers\API\Product\WishlistController;
use App\Http\Controllers\API\Promo\Flashsale\FlashsaleController;
use App\Http\Controllers\API\Promo\Voucher\VoucherController;
use App\Http\Controllers\API\User\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('auth')->middleware('api')->group(function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('logout', [AuthController::class, 'logout'])->middleware('isVerified')->name('logout');
    Route::post('me', [AuthController::class, 'me'])->middleware('isVerified')->name('me');
    Route::post('me/update', [UserController::class, 'updateUser'])->middleware('isVerified')->name('me.update');
    Route::post('me/update/password', [UserController::class, 'updatePassword'])->middleware('isVerified')->name('me.update.password');
    Route::post('me/update/fcm', [AuthController::class, 'saveToken'])->name('me.update.fcm');
    Route::post('me/setphone', [UserController::class, 'updatePhone'])->middleware('isVerified')->name('me.setphone');
    Route::prefix('forgot')->group(function () {
        Route::post('/', [AuthController::class, 'forgot'])->name('forgotPassword');
        Route::post('/verify', [AuthController::class, 'verifyForgot'])->name('verifyForgot');
        Route::post('/validate', [AuthController::class, 'validatePhone'])->name('validatePhone');
    });
    Route::prefix('register')->group(function () {
        Route::prefix('user')->group(function () {
            Route::post('/', [AuthController::class, 'register'])->name('register');
            Route::post('/verify', [AuthController::class, 'verify'])->name('verify');
            Route::post('/resend', [AuthController::class, 'resend'])->name('resend');
        });
    });
    Route::prefix('social')->group(function () {
        Route::post('/{provider}', [SocialAuthController::class, 'loginWithToken'])->name('loginWithToken');
    });
});

Route::prefix('category')->middleware('api')->group(function () {
    Route::get('/', [CategoryController::class, 'index'])->name('category.index');
    Route::post('/store', [CategoryController::class, 'store'])->middleware('isAdmin')->name('category.store');
    Route::post('/update/{category}', [CategoryController::class, 'update'])->middleware('isAdmin')->name('category.update');
    Route::delete('/delete/{category}', [CategoryController::class, 'destroy'])->middleware('isAdmin')->name('category.destroy');
});

Route::prefix('banner')->middleware('api')->group(function () {
    Route::get('/', [BannerController::class, 'index'])->name('banner.index');
    Route::post('/store', [BannerController::class, 'store'])->middleware('isAdmin')->name('banner.store');
    Route::post('/update/{banner}', [BannerController::class, 'update'])->middleware('isAdmin')->name('banner.update');
    Route::delete('/delete/{banner}', [BannerController::class, 'destroy'])->middleware('isAdmin')->name('banner.destroy');
});

Route::prefix('product')->middleware('api')->group(function () {
    Route::get('/', [ProductController::class, 'index'])->name('product.index');
    Route::post('/store', [ProductController::class, 'store'])->middleware('isAdmin')->name('product.store');
    Route::post('/update/{product}', [ProductController::class, 'update'])->middleware('isAdmin')->name('product.update');
    Route::delete('/delete/{product}', [ProductController::class, 'destroy'])->middleware('isAdmin')->name('product.destroy');
    Route::delete('/delete/sku/{sku}', [ProductController::class, 'destroySku'])->middleware('isAdmin')->name('product.destroy.sku');
    Route::prefix('wishlist')->middleware('isVerified')->group(function () {
        Route::post('/add', [WishlistController::class, 'addToWishlist'])->name('wishlist.add');
        Route::get('/list', [WishlistController::class, 'listWishlist'])->name('wishlist.list');
        Route::delete('/delete/{product}', [WishlistController::class, 'destroy'])->name('wishlist.destroy');
    });
    Route::prefix('review')->group(function () {
        Route::get('/detailed/{product}', [ReviewController::class, 'detailedReview'])->name('review.detailed');
        Route::post('/add/{product}', [ReviewController::class, 'addToReview'])->middleware('isVerified')->name('review.add');
        Route::get('/mylist', [ReviewController::class, 'listReview'])->middleware('isVerified')->name('review.list');
        Route::delete('/delete/{product}', [ReviewController::class, 'destroy'])->middleware('isVerified')->name('review.destroy');
    });
});

Route::prefix('order')->group(function () {
    Route::prefix('cart')->middleware('isVerified')->group(function () {
        Route::get('/', [CartController::class, 'index'])->name('cart.index');
        Route::post('/add', [CartController::class, 'addToCart'])->name('cart.add');
        Route::post('/delete', [CartController::class, 'removeFromCart'])->name('cart.destroy');
    });
    Route::prefix('ongkir')->group(function () {
        Route::get('/province', [OngkirController::class, 'province'])->name('ongkir.province');
        Route::get('/city/{province}', [OngkirController::class, 'city'])->name('ongkir.city');
        Route::get('/district/{city}', [OngkirController::class, 'district'])->name('ongkir.district');
        Route::get('/subdistrict/{district}', [OngkirController::class, 'subdistrict'])->name('ongkir.subdistrict');
        Route::get('/check', [OngkirController::class, 'checkOngkir'])->name('ongkir.check');
    });
    Route::prefix('customer')->middleware('isVerified')->group(function () {
        Route::post('/add', [CustomerController::class, 'addCustomer'])->name('customer.add');
        Route::get('/myaddress', [CustomerController::class, 'getMyAddress'])->name('customer.myaddress');
        Route::delete('/delete/{customer}', [CustomerController::class, 'deleteAddress'])->name('customer.destroy');
        Route::get('/primary/{customer}', [CustomerController::class, 'setPrimary'])->name('customer.primary');
        Route::post('/update/{customer}', [CustomerController::class, 'update'])->name('customer.update');
    });
    Route::post('place', [OrderController::class, 'place'])->middleware('isVerified')->name('place.order');
    Route::prefix('history')->middleware('isVerified')->group(function () {
        Route::get('/', [OrderController::class, 'myOrder'])->name('order.index');
        Route::get('/detail/{order}', [OrderController::class, 'detailOrder'])->name('order.detail');
        Route::post('/cancel/{order}', [OrderController::class, 'cancelOrder'])->name('order.cancel');
        Route::post('/complete/{order}', [OrderController::class, 'completeOrder'])->name('order.done');
        Route::get('/awb', [OrderController::class, 'getAwb'])->name('order.awb');
    });
    Route::prefix('admin')->middleware('isAdmin')->group(function () {
        Route::get('/', [OrderAdminController::class, 'index'])->name('order.admin');
        Route::post('/update/download', [OrderAdminController::class, 'updateToDownloaded']);
        Route::post('/update/process/{order}', [OrderAdminController::class, 'updateToOnProcess'])->name('order.process');
        Route::post('/update/delivery/{order}', [OrderAdminController::class, 'updateToOnDelivery'])->name('order.delivery');
        Route::post('/update/complete/{order}', [OrderAdminController::class, 'updateToComplete'])->name('order.complete');
        Route::post('/update/cancel/{order}', [OrderAdminController::class, 'updateToCancel'])->name('order.cancel');
        Route::get('/download/excel', [ExportController::class, 'exportOrder'])->name('order.download.excel');
    });
});

Route::prefix('user')->middleware('isAdmin')->group(function () {
    Route::get('/', [UserController::class, 'index'])->name('user.index');
    Route::post('/store', [UserController::class, 'store'])->name('user.store');
    Route::post('/update/{user}', [UserController::class, 'update'])->name('user.update');
    Route::delete('/delete/{user}', [UserController::class, 'destroy'])->name('user.destroy');
});

Route::prefix('promo')->group(function () {
    Route::prefix('flashsale')->group(function () {
        Route::get('/', [FlashSaleController::class, 'index'])->name('flashsale.index');
        Route::get('/mobile', [FlashSaleController::class, 'indexMobile'])->name('flashsale.mobile');
        Route::post('/store', [FlashSaleController::class, 'store'])->middleware('isAdmin')->name('flashsale.store');
        Route::post('/update/{flashsale}', [FlashSaleController::class, 'update'])->middleware('isAdmin')->name('flashsale.update');
        Route::delete('/delete/{flashsale}', [FlashSaleController::class, 'destroy'])->middleware('isAdmin')->name('flashsale.destroy');
        Route::prefix('item')->group(function () {
            Route::post('/update/{flashsale}', [FlashSaleController::class, 'updateFlashSaleItem'])->middleware('isAdmin')->name('flashsale.item.add');
        });
    });
    Route::prefix('voucher')->group(function () {
        Route::prefix('admin')->group(function () {
            Route::post('/store', [VoucherController::class, 'store'])->middleware('isAdmin')->name('voucher.store');
            Route::get('/', [VoucherController::class, 'index'])->middleware('isAdmin')->name('voucher.index');
            Route::post('/update/{voucher}', [VoucherController::class, 'update'])->middleware('isAdmin')->name('voucher.update');
            Route::delete('/delete/{voucher}', [VoucherController::class, 'destroy'])->middleware('isAdmin')->name('voucher.destroy');
        });
        Route::prefix('user')->group(function () {
            Route::get('/', [VoucherController::class, 'list'])->middleware('isVerified')->name('voucher.user.index');
            Route::post('/claim/{voucher}', [VoucherController::class, 'claim'])->middleware('isVerified')->name('voucher.user.claim');
            Route::get('claimed', [VoucherController::class, 'claimed'])->middleware('isVerified')->name('voucher.user.claimed');
        });
    });
});

Route::prefix('notification')->group(function () {
    Route::get('/', [NotificationController::class, 'index'])->name('notification.index');
    Route::post('/create', [NotificationController::class, 'store'])->middleware('isAdmin')->name('notification.store');
    Route::post('/update/{notification}', [NotificationController::class, 'update'])->middleware('isAdmin')->name('notification.update');
    Route::delete('/delete/{notification}', [NotificationController::class, 'destroy'])->middleware('isAdmin')->name('notification.destroy');
});

Route::prefix('brands')->group(function () {
    Route::get('/', [BrandController::class, 'index'])->name('brand.index');
    Route::post('/create', [BrandController::class, 'store'])->middleware('isAdmin')->name('brand.store');
    Route::post('/update/{brands}', [BrandController::class, 'update'])->middleware('isAdmin')->name('brand.update');
    Route::delete('/delete/{brands}', [BrandController::class, 'destroy'])->middleware('isAdmin')->name('brand.destroy');
});

Route::prefix('analytic')->middleware('isAdmin')->group(function () {
    Route::get('/', [AnalyticAdminController::class, 'analyticDashboard'])->name('analytic.index');
});
Route::post('midtrans/notification', [OrderController::class, 'notification'])->name('midtrans.notification');
Route::get('version', [MiscController::class, 'version'])->name('version');
