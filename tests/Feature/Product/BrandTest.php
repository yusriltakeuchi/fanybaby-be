<?php

namespace Tests\Unit\Http\Controllers\API\Product;

use Tests\TestCase;
use Illuminate\Http\Request;
use App\Http\Requests\product\BrandStoreRequest;
use App\Http\Controllers\API\Product\BrandController;
use App\Models\Product\Brands;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\JsonResponse;
use Mockery;

class BrandTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware();
    }
    public function testIndexMethod()
    {
        $brand1 = Brands::factory()->create(['name' => 'Brand 1']);
        $brand2 = Brands::factory()->create(['name' => 'Brand 2']);
        $brand3 = Brands::factory()->create(['name' => 'Brand 3']);

        $perPage = 20;
        $currentPage = 1;
        $totalItems = 3;
        $lastPage = 1;

        $paginator = new LengthAwarePaginator([$brand1, $brand2, $brand3], $totalItems, $perPage, $currentPage, [
            'path' => '',
            'pageName' => 'page',
        ]);

        $queryBuilderMock = Mockery::mock('alias:Spatie\QueryBuilder\QueryBuilder');
        $queryBuilderMock->shouldReceive('for')->once()->with(Brands::class)->andReturnSelf();
        $queryBuilderMock->shouldReceive('allowedFilters')->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('allowedSorts')->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('defaultSort')->once()->with('-id')->andReturnSelf();
        $queryBuilderMock->shouldReceive('paginate')->once()->with(20)->andReturn($paginator);

        $response = $this->json('GET', '/api/brands');

        $response->assertStatus(200)
            ->assertJson([
                'status' => 'success',
                'pagination' => [
                    'total' => $totalItems,
                    'per_page' => $perPage,
                    'current_page' => $currentPage,
                    'last_page' => $lastPage,
                    'from' => 1,
                    'to' => $totalItems,
                ],
            ])->assertJsonCount($totalItems, 'data');
    }

    public function testStoreMethod()
    {
        $requestMock = Mockery::mock(BrandStoreRequest::class);
        $requestMock->shouldReceive('validated')->once()->andReturn(['name' => 'New Brand', 'icon' => UploadedFile::fake()->image('file.jpg')]);
        $requestMock->shouldReceive('file')->once()->with('icon')->andReturn(UploadedFile::fake()->image('file.jpg'));

        $controller = new BrandController();
        $result = $controller->store($requestMock);

        $responseData = [
            'message' => 'Brand created',
            'data' => ['name' => 'New Brand', 'icon' => $result->getData()->data->icon, 'updated_at' => $result->getData()->data->updated_at, 'created_at' => $result->getData()->data->created_at, 'id' => $result->getData()->data->id],
            'status' => 'success',
        ];

        $expectedResponse = response()->json($responseData);

        $this->assertEquals($expectedResponse->getContent(), $result->getContent());
    }

    public function testUpdateMethod()
    {
        $brand = Brands::create(['name' => 'Original Brand', 'icon' => 'original-icon.jpg']);
        $requestMock = Mockery::mock(BrandStoreRequest::class);
        $requestMock->shouldReceive('validated')->once()->andReturn([
            'name' => 'Updated Brand',
            'icon' => UploadedFile::fake()->image('file.jpg')
        ]);
        $requestMock->shouldReceive('file')->once()->with('icon')->andReturn(UploadedFile::fake()->image('file.jpg'));
        $requestMock->shouldReceive('hasFile')->once()->with('icon')->andReturn(true);
        $controller = new BrandController();
        $response = $controller->update($brand, $requestMock);
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals('Brand updated', $response->getData()->message);
        $this->assertEquals('success', $response->getData()->status);
    }

    public function testDestroyMethod()
    {
        $brand = Brands::create(['name' => 'Brand to be deleted', 'icon' => 'icon.jpg']);

        $controller = new BrandController();

        $response = $controller->destroy($brand);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals('Brand deleted', $response->getData()->message);
        $this->assertEquals('success', $response->getData()->status);
    }
}
