<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('voucher', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('value');
            $table->enum('type', ['percent', 'nominal']);
            $table->integer('max_claim_total');
            $table->integer('max_claim_per_user');
            $table->string('type_of_use');
            $table->integer('min_total');
            $table->integer('max_discount');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('voucher');
    }
};
