<?php

namespace Database\Seeders\Banner;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Factories\Banner\BannerFactory;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BannerFactory::new()->count(10)->create();
    }
}
