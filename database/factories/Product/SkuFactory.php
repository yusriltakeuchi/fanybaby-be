<?php

namespace Database\Factories\Product;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class SkuFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected $model = \App\Models\Product\Sku::class;
    
    public function definition(): array
    {
        return [
            'product_id' => \App\Models\Product\Product::factory()->create()->id,
            'code' => $this->faker->text(10),
            'image' => 'https://picsum.photos/200/300',
            'price' => $this->faker->randomFloat(2, 0, 100),
            'properties' => json_encode([
                'color' => $this->faker->colorName,
                'size' => $this->faker->randomElement(['S', 'M', 'L', 'XL', 'XXL']),
            ]),
            'stock' => $this->faker->randomNumber(2),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
