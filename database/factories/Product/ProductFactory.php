<?php

namespace Database\Factories\Product;
use App\Models\Category\Category;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected $model = \App\Models\Product\Product::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'total_rating' => $this->faker->randomNumber(2),
            'rating' => $this->faker->randomFloat(2, 0, 5),
            'category_id' => Category::factory()->create()->id,
            'total_sell' => $this->faker->randomNumber(2),
            'image' => 'https://picsum.photos/200/300',
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
