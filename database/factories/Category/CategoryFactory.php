<?php

namespace Database\Factories\Category;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category\Category;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected $model = \App\Models\Category\Category::class;

    public function definition(): array
    {
        $is_parent = $this->faker->boolean;
        return [
            'name' => $this->faker->name,
            'icon' => 'https://picsum.photos/200/300',
            'is_featured' => $this->faker->boolean,
            'is_parent' => $is_parent,
            'parent_id' => $is_parent ? null : Category::where('is_parent', '1')->inRandomOrder()->limit(1)->first()->id,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
