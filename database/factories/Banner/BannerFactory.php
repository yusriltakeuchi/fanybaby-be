<?php

namespace Database\Factories\Banner;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Banner\Banner>
 */
class BannerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected $model = \App\Models\Banner\Banner::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'image' => 'https://picsum.photos/200/300',
            'link' => $this->faker->url,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
